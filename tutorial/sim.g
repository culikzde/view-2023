
/* sim.g */

/* ---------------------------------------------------------------------- */

/* expression */

ident_expr :
   identifier ;

int_value_expr  :
   number ;

float_value_expr  :
   real_number ;

string_value_expr :
   string_literal ;

nil_expr :
   "nil" ;

sub_expr :
   '(' expr ')' ;

not_expr :
   "not" factor ;

plus_expr :
   '+' factor ;

minus_expr :
   '-' factor ;

variable :
   ident_expr |
   sub_expr ;

reference :
   variable
   (
      index_expr |
      call_expr |
      deref_expr |
      field_expr
   )* ;

index_expr :
   '['
   expr ( ',' expr ) *
   ']'
   ;

call_expr :
   '('
   expr ( ',' expr ) *
   ')'
   ;

deref_expr :
   '^'
   ;

field_expr :
   '.'
   identifier
   ;

factor :
   int_value_expr |
   float_value_expr |
   string_value_expr  |
   not_expr |
   plus_expr |
   minus_expr |
   nil_expr |
   reference  ;

term :
  factor
  (
     (  '*'   |
        '/'   |
        "div" |
        "mod" |
        "and" )
     factor
  )* ;

simple_expr :
  term
  (
     (  '+'   |
        '-'   |
        "or"  |
        "xor" )
     term
  )* ;

expr :
  simple_expr
  (
     (  '='  |
        "<>" |
        '<'  |
        '>'  |
        "<=" |
        ">=" |
        "in" )
     simple_expr
  )? ;

/* ---------------------------------------------------------------------- */

/* statement */

stat_list :
  stat  ( ';'  stat )* ;

begin_stat :
  "begin"
  stat_list
  "end" ;

if_stat :
  "if" expr "then" stat ( "else" stat )? ;

while_stat :
  "while" expr "do" stat;

repeat_stat :
  "repeat"
  stat_list
  "until" expr ;

for_stat :
  "for" variable ":=" expr ( "to" | "downto" ) expr "do" stat ;

simple_stat  :
  expr
  (
     ":="
     expr
  |
     /* procedure call */
  ) ;

empty_stat : ;

stat :
  begin_stat |
  if_stat |
  while_stat |
  repeat_stat |
  for_stat |
  simple_stat |
  empty_stat;

/* ---------------------------------------------------------------------- */

param_item :
   identifier ;

param_decl :
  ( "var" | "const" | )
  param_item ( ',' param_item )*
  ':' param_type 
  ( '=' expr )? 
  ;

formal_param_list :
   '('
       (
          param_decl ( ';' param_decl )*
       )?
   ')';

param_type :
   alias_type |
   simple_string_type |
   simple_file_type ;

result_type :
   alias_type |
   simple_string_type ;

simple_string_type :
   "string" ;

simple_file_type :
   "file" ;
   
/* ---------------------------------------------------------------------- */

class_type :
   "class"
   (
     '('
     parent_type ( ',' interface_sect )?
     ')'
   )?
   components_sect
   "end" ;

interface_type :
  "interface"
   ( '(' interface_sect ')' )?
   components_sect 
   "end" ;

parent_type :
   alias_type ;

interface_sect :
   alias_type ( ',' alias_type )* ;

components_sect :
   member_sect
   ( components_item )* ;

components_item :
   ( "private"   |
     "protected" |
     "public"    )
   member_sect ;

member_sect :
   ( member_decl )* ;

member_decl :
   field_decl |
   method_decl ;

field_decl :
   identifier ( ',' identifier )*
   ':'
   type;

method_decl :
   proc_head
   (
     "virtual" |
     "override" |
     "abstract" 
   )* ;

/* ---------------------------------------------------------------------- */

enum_type :
   '(' identifier ( ',' identifier )* ')' ;

string_type :
   "string" ( '[' expr ']' )? ;

array_type :
   "array"
   ( '[' index_type_sect ']' )?
   "of"
   type;

index_type_sect :
   type ( ',' type )* ;

record_type :
   "record"
   field_sect
   "end"
   ;

field_sect :
   ( field_decl )* ;

pointer_type :
   '^' type;

set_type :
   "set" "of" type;

file_type :
   "file" ( "of" type )? ;

class_of_type :
  "class" "of" type ;

proc_type :
   ( "procedure" | "function" )
   formal_param_list
   ( ':' result_type )?
   ( "of" "object" )?
   ;

range_type :
   simple_expr
   ".."
   simple_expr
   ;

alias_type :
   identifier ( '.' identifier  )* ;

type :
   string_type |
   array_type |
   set_type |
   file_type |
   record_type |
   class_type |
   class_of_type |
   interface_type |
   proc_type |
   pointer_type |
   // ( range_type ) => range_type |
   // ( enum_type ) => enum_type |
   alias_type ;

/* ---------------------------------------------------------------------- */

/* labels */

label_decl :
   identifier | number ;

label_sect :
   "label" label_decl ( ',' label_decl )* ';' ;

/* constants */

const_decl :
   identifier 
   ( ':' type )? 
   '=' expr 
   ';' ;

const_sect :
   "const" const_decl ( const_decl )* ;

/* types */

type_decl :
   identifier '=' type ';' ;

type_sect :
   "type" type_decl ( type_decl )* ;

/* variables */

var_item  :
   identifier ;

var_decl :
   var_item ( ',' var_item )*
   ':' type
   ( '=' expr )?
   ';' ;

var_sect :
   "var" var_decl ( var_decl )* ;

/* subroutine */

proc_head :
   ( "class" )?

   ( "procedure"   |
     "function"    |
     "constructor" |
     "destructor"  )

   identifier
   formal_param_list
   ( ':' result_type ) ?

   ( "reintroduce" | "overload" )? 
   ';' ;

proc_decl :
   proc_head
   (
      "forward" 
   |
     decl_part
     "begin"
     stat_list
     "end"
   )
   ';' ;

proc_intf_decl :
   proc_head ;

/* declarations */

intf_decl :
   const_sect |
   type_sect |
   var_sect |
   proc_intf_decl ;

intf_decl_part :
   ( intf_decl )* ;

decl :
   label_sect |
   const_sect |
   type_sect |
   var_sect |
   proc_decl ;

decl_part :
   ( decl )* ;

/* import */

import_item :
  identifier
  ( "in" string_literal )? ;

import_sect :
  (
     "uses"
     import_item
     ( ',' import_item )*
     ';'
  )? ;

/* unit */

unit_decl :
   "unit" identifier ';'
   "interface"
   import_sect
   intf_decl_part
   "implementation"
   import_sect
   decl_part
   (
     ( "initialization" stat_list )?
     ( "finalization" stat_list )?
   |
     "begin"stat_list
   )?
   "end" '.' ;

/* program */

program_decl :
   "program" identifier ';'
   import_sect
   decl_part
   "begin"
   stat_list
   "end" '.' ;

/* library */

library_decl :
   "library" identifier ';'
   import_sect
   decl_part
   "begin"
   stat_list
   "end" '.' ;

/* module */

module_decl :
   unit_decl |
   program_decl |
   library_decl ;
