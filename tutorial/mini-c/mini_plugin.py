
# mini_plugin.py

from __future__ import print_function

from util import import_qt_modules
import_qt_modules (globals ())

from env import CodePlugin

# --------------------------------------------------------------------------

class Plugin (CodePlugin) :

   def __init__ (self, main_window) :
       super (Plugin, self).__init__ (main_window)

       menu = self.win.addTopMenu (self.mod.title)

       act = QAction ("Mini Make", self.win)
       act.triggered.connect (self.miniCompile)
       menu.addAction (act)

       act = QAction ("Mini Build", self.win)
       act.triggered.connect (self.miniBuild)
       menu.addAction (act)

   def miniCommon (self, rebuild) :
       self.setup ()
       self.start ("mini/examples/simple.cc")
       self.buildParser (rebuild)
       self.compile ()

   def miniCompile (self) :
       self.miniCommon (rebuild = False)

   def miniBuild (self) :
       self.miniCommon (rebuild = True)

   # -----------------------------------------------------------------------

   def setup (self) :
       "setup grammar, parser and compiler file name"
       self.grammarFileName = "mini/cmini.g"

       # self.compilerFileName = "cmini_parser.py"
       # self.compilerClassName = "Parser"
       self.compilerFuncName = "parse_declaration_list"

       self.enableMonitor = True

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
