
ebnf : "(" description ")" ( "?" | "*" | "+" | ) ;

simple : string_literal | identifier | ebnf ;

alternative : ( simple )* ;

description : alternative ( "|" alternative )* ;

rule : identifier ":" description ";" ;

grammar : ( rule )* ;

