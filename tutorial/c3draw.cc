
RectangleShape rectangle
{
    x = 0;
    y = 0;
    width = 10000;
    height = 5000;
    text = "My new RectangleShape";
    CornerRadius = 1000;
    Shadow = True;
    ShadowXDistance = 250;
    ShadowYDistance = 250;
    FillColor = "0000C0";
    LineColor = "C00000";
    Name = "Rounded Gray Rectangle";
}

