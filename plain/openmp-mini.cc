#include <stdio.h>
#include <omp.h>
#include <cmath>
#include <vector>
#include <iostream>

int main()
{
    const int size = 1000;
    std::vector<double> sinTable(size);
    std::vector<double> cosTable(size);
    double sum = 0;

    #pragma omp parallel for reduction (+:sum)
    for(int n=0; n<size; ++n)
    {
      sinTable[n] = std::sin(2 * M_PI * n / size);
      cosTable[n] = std::cos(2 * M_PI * n / size);
      double t = sinTable[n]*sinTable[n] + cosTable[n] * cosTable[n];
      sum = sum + t;
      std::cout << sinTable[n] << " "  << cosTable[n] << " " << t << '\n';
    }
    std::cout << "sum " << sum << '\n';

    // the table is now initialized
}

/* ---------------------------------------------------------------------- */

// https://stackoverflow.com/questions/51064975/nvptx-gcc-9-0-0-trunk-for-openmp-4-5-off-loading-to-gpu-device-cannot-find-l

// compile: -fopenmp -lstdc++ -lm

// pacman -S openmp

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
