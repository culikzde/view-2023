test -f Makefile && make clean
python3 configure.py || exit 1

# Debian 11 and 12 problem
sed -i 's/$(LINK) $(LFLAGS) -o $(TARGET) $(OFILES) $(LIBS)/$(LINK) -o $(TARGET) $(OFILES) $(LFLAGS) $(LIBS)/' Makefile

make || exit 1
python3 run.py

# dnf install python3-sip-devel python3-qt5-devel qt5-tools-devel
