
/* button.h */

#ifndef BUTTON_H
#define BUTTON_H

#include <QPushButton>

/* ---------------------------------------------------------------------- */

class Button : public QPushButton {
   public:
      Button (QWidget * parent = 0) : QPushButton (parent) { setText ("My Button"); }
};

#endif // BUTTON_H
