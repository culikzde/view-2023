#!/usr/bin/env python

import os, sys

import inspect

import util

# --------------------------------------------------------------------------

def describe_class (win, target, name, obj) :
    result = win.treeItem (target, name)
    result.addIcon ("class")
    result.obj = obj
    add_location (win, result, obj)
    describe_members (win, result, obj)

def describe_function (win, target, name, data) :
    result = win.treeItem (target, name)
    result.addIcon ("function")
    result.obj = data
    add_location (win, result, data)
    (args, varargs, keywords, defaults) = inspect.getargspec (data)
    for arg in args :
        param = win.treeItem (result, arg)

def describe_other (win, target, name, data) :
    result = win.treeItem (target, name)
    result.obj = data
    add_location (win, result, data)

def add_location (win, result, data) :
    if hasattr (data, "co_filename") :
       data.src_file = win.fileNameToIndex (data.co_filename)
    if hasattr (data, "co_firstlineno") :
       data.src_line = data.co_firstlineno

def describe_members (win, target, data) :
    members = inspect.getmembers (data)
    for member in members :
        (name, obj) = member
        if name.startswith ("__") :
           pass
        elif inspect.isbuiltin (obj) :
           pass
        elif inspect.isclass (obj) :
           describe_class (win, target, name, obj)
        elif inspect.ismethod (obj) or inspect.isfunction (obj) :
           describe_function (win, target, name, obj)
        else :
           describe_other (win, target, name, obj)

def describe_module (win, target, data) :
    name = data.__name__
    result = win.treeItem (target, name)
    result.addIcon ("function")
    describe_members (win, result, data)

def py_inspect () :
    win = util.get_win ()
    result = win.treeBranch ("qt module")
    for module in util.qt_modules :
        describe_module (win, result, module)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
