
#include "QApplication"

#include "QObject"
#include "QWidget"
#include "QAbstractButton"
#include "QPushButton"
#include "QLineEdit"
#include "QTreeWidget"

int main (int argc, char * * argv)
{
     QApplication appl (argc, argv);

     QWidget * window = new QWidget ();
     window->resize (320, 240);
     window->show ();

     QPushButton * button = new QPushButton ("Press me", window);
     button->move (100, 100);
     button->show ();

     appl.exec ();
}
