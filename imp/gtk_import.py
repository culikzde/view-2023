#!/usr/bin/env python

from __future__ import print_function

# import pygtk
# pygtk.require('2.0')
# import gtk as Gtk

import gi
gi.require_version ("Gtk", "3.0")
from gi.repository import Gtk

import os, sys, inspect

from code import *
from code_cls import *

# --------------------------------------------------------------------------

def named_type (text) :
    result = NamedType ()
    result.type_label = text
    return result

# --------------------------------------------------------------------------

def displayClass (target, name, obj) :
    result = Class ()
    result.item_name = name
    result.comment = obj.__doc__
    target.add (result)
    # displayMembers (result, obj)
    if hasattr (obj, "__info__") :
       if hasattr (obj.__info__, "get_fields") :
          displayInfo (result, obj.__info__.get_fields ())
       if hasattr (obj.__info__, "get_properties") :
          displayInfo (result, obj.__info__.get_properties ())
       if hasattr (obj.__info__, "get_methods") :
          displayFunctionInfo (result, obj.__info__.get_methods ())
       if 0 :
          print (name)
          members = inspect.getmembers (obj.__info__)
          for member in members :
             (name, item) = member
             print (name)
          if 0 :
             print ("name:", obj.__info__.get_name ())
             print ("parent:", obj.__info__.get_parent ())
             print ()
             print ("interfaces:", obj.__info__.get_interfaces ())
             print ()
             print ("constants:", obj.__info__.get_constants ())
             print ()
             print ("fields:", obj.__info__.get_fields ())
             print ()
             print ("properties:", obj.__info__.get_properties ())
             print ()
             print ("methods:", obj.__info__.get_methods ())
             print ()
             print ("signals:", obj.__info__.get_signals ())
             print ()
             print ("vfuncs:", obj.__info__.get_vfuncs ())
             print ()

def displayFunctionInfo (target, data) :
    for item in data :
        result = Function ()
        result.item_name = item.get_name ()
        result.comment = item.__doc__
        # if hasattr (item, "get_return_type") :
        #    result.result_type = named_type (str (item.get_return_type ())) # !?
        target.add (result)
        if hasattr (item, "get_arguments") :
           for arg in item.get_arguments () :
               param = Variable ()
               param.item_name = arg.get_name ()
               # !? result.parameters.add (param)

def displayInfo (target, data) :
    for item in data :
        result = Declaration ()
        result.item_name = item.get_name ()
        if hasattr (item, "get_type") :
           result.item_type = named_type (item.get_type ()) # !?
        result.comment = item.__doc__
        target.add (result)

def displayFunction (target, name, data) :
    result = Function ()
    result.item_name = name
    # !? result.comment = item.__doc__
    target.add (result)
    if 1 :
       # try :
          (args, varargs, keywords, defaults) = inspect.getargspec (data)
          for arg in args :
              param = Variable ()
              param.item_name = arg
              if hasattr (arg, "get_type") :
                 param.item_type = named_type (arg.get_type ()) # !?
              # result.parameters.add (param)
       # except :
          pass

def displayOther (target, name, data) :
    result = Declaration () # !?
    result.item_name = name
    result.comment = data.__doc__
    target.add (result)

def displayMembers (target, data) :
    members = inspect.getmembers (data)
    for member in members :
        (name, obj) = member
        if name.startswith ("__") :
           # print (name)
           pass
        elif inspect.isbuiltin (obj) :
           pass
        elif inspect.isclass (obj) :
           displayClass (target, name, obj)
        elif inspect.ismethod (obj) or inspect.isfunction (obj) :
           displayFunction (target, name, obj)
        else :
           displayOther (target, name, obj)

def displayModule (data) :
    result = Namespace ()
    result.item_name = "Gtk Classes"
    displayMembers (result, data)
    return result

def displayGtk (win) :
    data = displayModule (gi.repository.Gtk)
    win.showClasses (data)
    win.showTab (win.classes)

# --------------------------------------------------------------------------

# see pygobject source (version 2 or 3)
# gi/pygi-info.c

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
