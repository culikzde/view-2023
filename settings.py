# settings.py

from __future__ import print_function

import os, inspect

from lexer import Lexer

# --------------------------------------------------------------------------

class SettingsData (object) : # Python 2.7 requires (object)
   pass

# --------------------------------------------------------------------------

class CommandData (SettingsData) :
   _name_field_ = "title"
   _read_note_ = True # read qmake parameters
   _user_fields_ = [ ] #
   def __init__ (self) :
       super (CommandData, self).__init__ ()

       self.title = ""
       self.icon = ""
       self.shortcut = ""
       self.tooltip = ""
       self.toolbar = 0 # position in toolbar

       self.cd = "" # current directory
       self.set = [] # set environment variables
       self.env = [] # environment names

       self.cmd = "" # shell command
       self.python_cmd = False # True => use Python executable as cmd
       self.jscript = "" # java script
       self.url = [] # web

       self.module = "" # Python module
       self.cls = "" # class
       self.func = "" # function

       self.load = [ ] # pre-load modules
       self.loaded_module = "" # already loaded module

       self.plugin = "" # parameters for builder from env.py
       self.param = ""


       self.enableMonitor = False
       self.fast = False
       self.ignore_all_includes = False
       self.clang_import = False
       self.compilerOptions = ""

       self.compilerFileName = ""
       self.compilerClassName = ""
       self.compilerFuncName = ""
       self.cppClassName = ""
       self.pythonClassName = ""
       self.outputSuffix = ""
       self.sourceFileNames = [ ]

       self.ext = "" # valid only for this file extension

       self.addWin = False # add main window as parameter
       self.addBuilder = False # add builder object as parameter
       self.askFileName = "" # show file dialog
       self.addFileName = False # add file name as parameter

       # private variable
       self._notes_ = { }
       self._action_ = None

       if len (self._user_fields_) == 0 :
          members = inspect.getmembers (self)
          for member in members :
             (name, obj) = member
             if not inspect.isroutine (obj) :
                if not name.startswith ('_') :
                   self._user_fields_.append (name)
          self._user_fields_.append ("_notes_")
          # print ("USER FIELDS", self._user_fields_)

qmake_notes = [
                "TARGET",
                "QT",
                "CONFIG",

                "HEADERS",
                "PRECOMPILED_HEADER",
                "SOURCES",
                "FORMS",
                "RESOURCES",
                "SUBDIRS",

                "DEFINES",
                "INCLUDEPATH",
                "OBJECTS",
                "LIBS",
                "PKGCONFIG",
                "DISTFILES",
                "DESTDIR",

                "QMAKE_CC",
                "QMAKE_CXX",
                "QMAKE_EXT_H",
                "QMAKE_EXT_CC",
                "QMAKE_LINK",
                "QMAKE_CFLAGS",
                "QMAKE_CXXFLAGS",
                "QMAKE_LFLAGS",
                "QMAKE_CLEAN",
                "QMAKE_RPATHDIR",
              ]

# --------------------------------------------------------------------------

"""
class ParserData (SettingsData) :
   def __init__ (self) :
       super (ParserData, self).__init__ ()

       self.input = "" # input file name
       self.lang = "" # output language : python, c, ...

       self.ns = "" # namespace
       self.lexer_header = "" # lexer header name

       # output file names
       self.header = ""
       self.code = ""
       self.html = ""
       self.lout = ""

class ProductData (SettingsData) :
   def __init__ (self) :
       super (ProductData, self).__init__ ()

       self.input = "" # input file name
       self.lang = "" # output language : python, c, ...

       self.ns = "" # namespace
       self.parser_header = "" # lexer header name

       # output file names
       self.header = ""
       self.code = ""

class ProcessingData (SettingsData) :
   def __init__ (self) :
       super (ProcessingData, self).__init__ ()
"""

# --------------------------------------------------------------------------

class PluginData (SettingsData) :

   _name_dict_ = { "module" : "module_name",
                   "cls" : "cls_name" } # rename fields

   _name_list_ = [ "title", "key", "reread_menu", "reload_module" ] # other valid fields

   _name_field_ = "title" # field after type name

   def __init__ (self) :
       super (PluginData, self).__init__ ()

       self.title = "" # menu title
       self.key = "" # plugin identifier
       self.module_name = "" # Python module
       self.cls_name = "" # Python class

       self.reread_menu = False # refresh menu on opening menu
       self.reload_module = False # reload module on menu click

       # private variables
       self.module = None
       self.cls = None
       self.menu = None

class MenuData (SettingsData) :
   _name_field_ = "title"
   def __init__ (self) :
       super (MenuData, self).__init__ ()
       self.title = ""
       self.plugin = "" # plugin name for following menu items
       self.middle = False # middle ... insert after plugin menu

class MenuItem (CommandData) :
   _name_field_ = "title"
   _read_note_ = True # read qmake parameters
   def __init__ (self) :
       super (MenuItem, self).__init__ ()

class MenuSeparator (SettingsData) :
   def __init__ (self) :
       super (MenuSeparator, self).__init__ ()

class ShortcutData (SettingsData) :
   def __init__ (self) :
       super (ShortcutData, self).__init__ ()
       self.name = ""
       self.icon = ""
       self.shortcut = ""
       self.toolbar = 0

       # private variable
       self._action_ = None

class OptionPage (SettingsData) :
   _name_field_ = "name"
   def __init__ (self) :
       super (OptionPage, self).__init__ ()
       self.name = ""
       self.icon = ""
       self.items = [ ]

class OptionTab (SettingsData) :
   _name_field_ = "name"
   def __init__ (self) :
       super (OptionTab, self).__init__ ()
       self.name = ""
       self.items = [ ]
       # private variable
       self._tab_ = True

class OptionEdit (SettingsData) :
   _name_field_ = "name"
   def __init__ (self) :
       super (OptionEdit, self).__init__ ()
       self.name = ""
       self.value = ""
       self.file_dialog = False

class OptionComboBox (SettingsData) :
   _name_field_ = "name"
   def __init__ (self) :
       super (OptionComboBox, self).__init__ ()
       self.name = ""
       self.value = ""
       self.values = [ ]
       self.file_dialog = False

       # private variable
       self._combo_box_ = True
       self._data_ref_ = None
       self._field_name_ = ""

class OptionCheckBox (SettingsData) :
   _name_field_ = "name"
   def __init__ (self) :
       super (OptionCheckBox, self).__init__ ()
       self.name = ""
       self.value = False

       # private variable
       self._check_box_ = True

class ColorData (SettingsData) :
   def __init__ (self) :
       super (ColorData, self).__init__ ()
       self.name = ""
       self.value = ""

class ColorTableData (SettingsData) :
   _read_list_ = True # add strings to 'items' list
   def __init__ (self) :
       super (ColorTableData, self).__init__ ()
       self.name = ""
       self.items = [ ]

class IconData (SettingsData) :
   _read_dict_ = True # add names and values to 'items' dictionary
   def __init__ (self) :
       super (IconData, self).__init__ ()
       self.path = ""
       self.items = { }

class PythonPathData (SettingsData) :
   _read_list_ = True # add strings to 'items' list
   def __init__ (self) :
       super (PythonPathData, self).__init__ ()
       self.items = [ ]

class SetupFuncData (SettingsData) :
   _read_code_ = True # add lines to 'code' variable
   def __init__ (self) :
       super (SetupFuncData, self).__init__ ()
       self.code = ""

class OpenFileData (SettingsData) :
   def __init__ (self) :
       super (OpenFileData, self).__init__ ()
       self.name = ""

class ConfigInfo (object) :
   def __init__ (self) :
       super (ConfigInfo, self).__init__ ()
       self.file_name = ""
       self.time_stamp = None

# --------------------------------------------------------------------------

class CmdData (SettingsData) :
   def __init__ (self) :
       super (SettingsData, self).__init__ ()
       self.cc = "g++"
       self.gdb = "gdb"
       self.nvcc = "nvcc"
       self.vcvars = "vcvars"
       self.git = "vcvars"
       self.qtcreator = "qtcreator"
       self.notepad = "notepad++"
       self.ssh = "ssh"
       self.scp = "scp"

"""
   _pages_ = " " "
      OptionPage
      {
         name = Compiler
         icon = edit-undo

         OptionTab
         {
            name = Linux

            OptionComboBox
            {
               name = cc
               value = gcc
               values = gcc
               values = g++
               values = clang
               values = clang++
               file_dialog = 1
            }

            OptionComboBox cc_options
            {
               values = "-g"
            }
         }

         OptionTab Windows
         {
            OptionEdit vcvars
            {
               value = set_vars.bat
               file_dialog = 1
            }
         }
      }

       OptionPage Build
       {
           OptionEdit qmake
       }

       OptionPage Debug
       {
           OptionEdit gdb
           {
              value = gdb
              file_dialog = 1
           }

           OptionEdit gdb_options
           {
              value = gdb
              file_dialog = 1
           }
       }

       OptionPage Edit
       {
           OptionEdit qtcreator
           OptionEdit notepad
       }

       OptionPage Remote
       {
          icon = folder-new

          OptionComboBox ssh
          {
              value = ssh
              // values = [ "scp", "ssh" ]
              values = "scp"
              values = "ssh"
              file_dialog = 1
          }

          OptionEdit ssh_options
          {
          }
       }
       " " "
"""

"""
   _pages_ = [
               ["Compile",  [
                              ["Linux", ["cc"]],
                              ["Windows", ["vcvars"]]
                            ]],
               ["Build",    ["qmake"]],
               ["Debug",    ["gdb"]],
               ["Edit",     ["qtcreator", "notepad"]],
               ["Remote",   ["ssh", "scp"]],
             ]

def addField (target, data, field) :
    combo = OptionComboBox ()
    combo.name = field
    combo.value = getattr (data, field, "")
    combo._data_ref_ = data
    combo._field_name_ = field
    target.append (combo)

def addFields (page, tab_name, data, field_list) :
    tab = OptionTab ()
    tab.name = tab_name
    for field in field_list :
        addField (tab.items, data, field)
    page.items.append (tab)

def addPages (conf, data) :
     for page_desc in data._pages_ :
         cnt = len (page_desc)
         page = OptionPage ()
         page.name = page_desc [0]
         conf.page_list.append (page)
         tabs_desc = page_desc [1]
         if isinstance (tabs_desc [0], str) :
            "only field names"
            addFields (page, "", data, tabs_desc)
         else :
            "tabs"
            for tab_desc in tabs_desc :
                addFields (page, tab_desc [0], data, tab_desc [1])
"""

# --------------------------------------------------------------------------

class ConfigData (object) :
   def __init__ (self) :
       super (ConfigData, self).__init__ ()

       self.win = None

       self.reread = False # True ... reread data from files

       # configuration files
       self.tool_info = ConfigInfo ()
       self.proj_info = ConfigInfo ()

       self.cmd_data = CmdData ()

       # initial values
       self.initial_color_cache = { }
       self.initial_icon_path = [ ]
       self.initial_icon_cache = { }

       # removable items
       self.dynamic_module_path = [ ]
       self.dynamic_menu_items = [ ]
       self.dynamic_toolbar_items = [ ]

       # data from files
       self.clear ()

   def clear (self) :
       self.plugin_list = [ ]
       self.command_list = [ ]
       self.menu_list = [ ]
       self.page_list = [ ]
       self.color_list = [ ]
       self.color_table_list = [ ]
       self.icon_list = [ ]
       self.shortcut_list = [ ] # refresh not implemented
       self.path_list = [ ]
       self.setup_list = [ ] # used only during window creation
       self.open_list = [ ]

       "read CmdData._pages_"
       # inp = Lexer ()
       # inp.openString (self.cmd_data._pages_)
       # while not inp.isEndOfSource () :
       #    readSetting (self, inp)
       # inp.close ()

# --------------------------------------------------------------------------

typeNameDict = {
                 "Command"    : "CommandData",
                 "Plugin"     : "PluginData",
                 "Menu"       : "MenuData",
                 "Shortcut"   : "ShortcutData",
                 "Color"      : "ColorData",
                 "ColorTable" : "ColorTableData",
                 "Icons"      : "IconData",
                 "PythonPath" : "PythonPathData",
                 "SetupFunc"  : "SetupFuncData",
                 "OpenFile"   : "OpenFileData",
                } # translate type names

def findClass (type_name) :
    cls = None
    if type_name in typeNameDict :
       type_name = typeNameDict [type_name] # rename
    cls = globals().get (type_name, None)
    if cls != None and not issubclass (cls, SettingsData):
       cls = None
    return cls

def isTypeName (type_name) :
    cls = findClass (type_name)
    return cls != None

def createObject (type_name) :
    obj = None
    cls = findClass (type_name)
    if cls != None:
       obj = cls ()
    return obj

def renameField (obj, name) :
    if name.startswith ('_') or name == "items" :
       name = ""
    else :
       if hasattr (obj, "_name_dict_") and name in obj._name_dict_ :
          name = obj._name_dict_ [name] # rename
       else :
          if hasattr (obj, "_name_list_") :
             if name not in obj._name_list_ :
                name = ""
          else :
             if not hasattr (obj, name) :
                name = ""
    return name

def readBoolValue (input) :
    if input.isKeyword ("true") or input.isKeyword ("True") or input.isNumber () and input.tokenText == "1" :
       input.nextToken ()
       value = True
    elif input.isKeyword ("false") or input.isKeyword ("False") or input.isNumber () and input.tokenText == "0":
       input.nextToken ()
       value = False
    else :
       input.error ("Boolean expected")
    return value

def readSimpleString (input) :
    if input.isIdentifier () :
       txt = input.readIdentifier ()
    else :
       txt = input.readString ()
    return txt

def readStringValue (input) :
    if input.isIdentifier () :
       txt = input.tokenText
       while input.ch > ' ' and input.ch not in [ ',', ';', '{', '}', '[', ']' ] :
          txt = txt + input.ch
          input.nextChar ()
       input.nextToken ()
    else :
       txt = input.readString ()
    return txt

def readValue (input, obj, name) :
    orig = getattr (obj, name, None)

    if isinstance (orig, bool) :
       value = readBoolValue (input)
       setattr (obj, name, value)
    elif isinstance (orig, int) :
       value = input.readSignedNumber ()
       setattr (obj, name, value)
    elif isinstance (orig, float) :
       value = input.readSignedReal ()
       setattr (obj, name, value)
    elif isinstance (orig, str) :
       value = readStringValue (input)
       setattr (obj, name, value)
    elif isinstance (orig, list) :
       value = readStringValue (input)
       orig.append (value)
    else :
       input.error ("Unknown field: " + name)

    if input.isSeparator (';') :
       input.nextToken ()

def readList (input) :
    result = [ ]
    if input.isSeparator ('[') :
       input.checkSeparator ('[')
       while not input.isSeparator (']') :
          value = readStringValue (input)
          result.append (value)
          if input.isSeparator (',') :
             input.nextToken ()
       input.checkSeparator (']')
    else :
       value = readStringValue (input)
       result.append (value)
    return result

def readNote (input, obj, name) :
    input.check ('=')

    orig = obj._notes_.get (name, [ ])
    value = self.readList ()
    orig = orig + value

    obj._notes_ [name] = orig
    print ("NOTE", name, orig)

    if input.isSeparator (';') :
       input.nextToken ()

def readNestedBlock (input, target, type_name) :
    obj = readBlock (input, type_name)
    ok = False
    if isinstance (target, OptionPage) :
       if isinstance (obj, OptionTab) :
          target.items.append (obj)
          ok = True
       if isinstance (obj, OptionEdit) or isinstance (obj, OptionComboBox) or isinstance (obj, OptionCheckBox):
          target.items.append (obj)
          ok = True
    if isinstance (target, OptionTab) :
       if isinstance (obj, OptionEdit) or isinstance (obj, OptionComboBox) or isinstance (obj, OptionCheckBox) :
          target.items.append (obj)
          ok = True
    if not ok :
       input.error ("Cannot add " + type_name + " into " + target.__class__.__name__)

def readCodeBlock (input) :
    if input.tokenText != '{' :
       input.error ("{ expected")

    "skip white spaces and end of line"
    while input.ch != '\0' and input.ch != '\r'  and input.ch != '\n' and input.ch <= ' ' :
        input.nextChar ()
    if input.ch == '\r' :
       input.nextChar ()
    if input.ch == '\n' :
       input.nextChar ()

    code = ""
    stop = False
    while not stop :
       line = ""
       empty = True
       while input.ch != '\0' and input.ch != '\r' and input.ch != '\n' and not stop:
          if empty :
             if input.ch > ' ' :
                empty = False
             if input.ch == '}' :
                stop = True # line starting with }
          line += input.ch
          # print ("CHAR", input.ch)
          input.nextChar ()

       if input.ch == '\r' :
          input.nextChar ()
       if input.ch == '\n' :
          input.nextChar ()

       if not stop :
          # print ("LINE", line)
          code = code + line + "\n"

    input.nextToken () # skip }
    return code

def readBlock (input, type_name) :
    # print ("READ BLOCK", type_name)
    if not isTypeName (type_name) :
       input.error ("Unknown type name: " + type_name)
    obj = createObject (type_name)

    if input.isIdentifier () or input.isString () :
       name = getattr (obj, "_name_field_", "") # declared name field
       if name == "" :
          name = renameField (obj, "name") # standard name field
       if name != "" :
          # print ("READ NAME", type_name)
          value = readSimpleString (input)
          setattr (obj, name, value)

    if input.isSeparator (';') :
       input.nextToken ()
    elif input.isSeparator ('{') :
       read_list = getattr (obj, "_read_list_", False)
       read_dict = getattr (obj, "_read_dict_", False)
       read_code = getattr (obj, "_read_code_", False)
       read_note = getattr (obj, "_read_note_", False)

       if read_code :
          obj.code = readCodeBlock (input)
       else :
          input.check ('{')
          while not input.isSeparator ('}') :
             if read_code :
                value = readStringValue (input)
             if read_list :
                value = readList (input)
                obj.items = obj.items + value
                if input.isSeparator (',') or input.isSeparator (';') :
                   input.nextToken ()
             else :
                name = input.readIdentifier ()
                if not read_dict and isTypeName (name) :
                   readNestedBlock (input, obj, name)
                elif read_note and name in qmake_notes :
                   readNote (input, obj, name)
                else :
                   orig_name = name
                   name = renameField (obj, name)
                   if name != "" :
                      input.check ('=')
                      readValue (input, obj, name)
                   else  :
                      name = orig_name
                      if read_dict :
                         input.check ('=')
                         value = readStringValue (input)
                         obj.items [name] = value
                      else :
                         input.error ("Unknown field: " + name)

          input.check ('}')

    return obj

def readCmdBlock (input, conf) :
    input.checkSeparator ('{')
    while not input.isSeparator ('}') :
        name = input.readIdentifier ()
        input.checkSeparator ('=')
        value = readStringValue (input)

        # print ("CMD", name, "=", value)
        found = False
        for page in conf.page_list :
            for tab in page.items :
                for item in tab.items :
                    if isinstance (item, OptionComboBox) :
                       if item.name == name :
                           # print ("FOUND", name)
                           found = True
                           item.values.append (value)
                           if item.value == "" :
                              item.value = value

        if not found :
           print ("MISSING CMD", name)

        if input.isSeparator (';') :
           input.nextToken ()
    input.checkSeparator ('}')

def readSetting (conf, input) :
     type_name = input.readIdentifier ()
     if type_name == "Cmd" :
        readCmdBlock (input, conf)
     else :
        obj = readBlock (input, type_name)
        if isinstance (obj, MenuData) or isinstance (obj, MenuItem) or isinstance (obj, MenuSeparator) : # before CommandData
           conf.menu_list.append (obj)
        elif isinstance (obj, CommandData) :
           conf.command_list.append (obj)
        elif isinstance (obj, PluginData) :
           conf.plugin_list.append (obj)
        elif isinstance (obj, ShortcutData) :
           conf.shortcut_list.append (obj)
        elif isinstance (obj, OptionPage) :
           conf.page_list.append (obj)
        elif isinstance (obj, ColorData) :
           conf.color_list.append (obj)
        elif isinstance (obj, ColorTableData) :
           conf.color_table_list.append (obj)
        elif isinstance (obj, IconData) :
           conf.icon_list.append (obj)
        elif isinstance (obj, PythonPathData) :
           conf.path_list.append (obj)
        elif isinstance (obj, SetupFuncData) :
           conf.setup_list.append (obj)
        elif isinstance (obj, OpenFileData) :
           conf.open_list.append (obj)
        else :
           input.error ("Invalid settings item: " + type_name)

# --------------------------------------------------------------------------

def readData (conf, fileName) :
    input = Lexer ()
    input.openFile (fileName)
    while not input.isEndOfSource () :
        readSetting (conf, input)
    input.close ()

def readAdditionalConfig (conf, input) :
    "read config from source file"
    while not input.isEndOfSource () and not input.isSeparator ('}') :
        readSetting (conf, input)
    conf.reread = True # next readConfig reread all files

def readCommandConfig (input) :
    "read config from source file"
    return readBlock (input, "Command")

def readConfig (conf) :
    result = False

    if conf.reread :
       result = True # read from files
       conf.reread = False

    # check time stamps
    for info in [conf.tool_info, conf.proj_info ] :
       if info.file_name != "" :
          time_stamp = os.path.getmtime (info.file_name)
          if info.time_stamp != time_stamp :
             result = True # new configuration

    # read files
    if result :
       conf.clear () # clear original lists, keep dynamic_...
       # addPages (conf, conf.cmd_data)
       for info in [conf.tool_info, conf.proj_info ] :
          if info.file_name != "" :
             info.time_stamp = None # forget old time stamp
             readData (conf, info.file_name)

    return result

def initConfig (file_name) :
    conf = ConfigData ()
    conf.tool_info.file_name = file_name
    conf.proj_info.file_name = ""

    conf.cmd_data = CmdData ()

    readConfig (conf)
    return conf

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
