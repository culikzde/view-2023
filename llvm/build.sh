#!/bin/sh

use_sip6=false

if test -f /etc/arch-release ; then
   use_sip6=true
fi

if grep 36 /etc/fedora-release 2>/dev/null >/dev/null ; then
   use_sip6=true
fi

if grep 37 /etc/fedora-release 2>/dev/null >/dev/null ; then
   use_sip6=true
fi

if $use_sip6 ; then

   sip-build --verbose --target-dir=. --no-make || exit 1

   cd _build && make && cd .. || exit 1
   cp _build/compiler/libcompiler.so compiler.so || exit 1

   # required file pyproject.toml
   python run.py

   # pacman -S sip (python-pyqt5-sip) python-pyqt5 pyqt-builder
   # conflict with sip4 python-sip4

   # dnf install python3-qt5-devel PyQt-builder (sip6) python3-devel
   # dnf install clang-devel llvm-devel

   # Fedora 36: see python3-poppler-qt5- ... .fc36.src.rpm
   # Archlinux: see python-poppler-qt5, http://github.com/frescobaldi/python-poppler-qt5/blob/master/pyproject.toml

   # https://www.riverbankcomputing.com/static/Docs/PyQt-builder/pyproject_toml.html

   # file pyproject.tohtml :
   # sip-include-dirs =  [ ...  "/usr/lib64/python3.11/site-packages/PyQt5/bindings" ...]
   # libraries = ["LLVM-15", "clang-cpp"]

else

   test -f Makefile && make clean
   python3 configure.py || exit 1

   # Debian 11 problem
   sed -i 's/$(LINK) $(LFLAGS) -o $(TARGET) $(OFILES) $(LIBS)/$(LINK) -o $(TARGET) $(OFILES) $(LFLAGS) $(LIBS)/' Makefile

   make || exit 1
   python3 run.py

   # dnf install python3-sip-devel
   # dnf install clang-devel llvm-devel

   # apt-get install python3-pyqt5 python3-sip-dev pyqt5-dev python-is-python3
   # apt-get install llvm clang libclang-dev libclang-cpp-dev (libclang11-cpp-dev)
   # apt-get install make pkgconf g++ qtbase5-dev

   # pacman -S sip
   # pacman -S python-sip
   # pacman -S clang

   # Fedora: ModuleNotFoundError: No module named 'sip'
   #  or Unable to find file "QtGui/QtGuimod.sip"
   # dnf install python3-qt5-devel
   # apt-get install pyqt5-dev
   # pacman -S python-pyqt5 python-pyqt5-sip

   # Debian 10, clang 7
   # apt-get install libclang-7.0-dev

   # Debian 10, clang 11 from backports
   # apt-get install libclang-11-dev clang-11

fi
