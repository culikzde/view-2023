/* win.t */

/* ---------------------------------------------------------------------- */

#if 1

#ifndef IGNORE

parameters
{
   cppParserFileName = "cmm_parser.cpp"
   cppProductFileName = "cmm_product.cpp"
}

build
{
compilerOptions = "-D IGNORE"

// compilerClassName = CmmCompiler
// cppClassName = ToCpp
// pythonClassName = ToPy
// pythonClassName = ToExtPy

// compilerClassName = CmmQtCompiler
// cppClassName = ToQtCpp
// pythonClassName = ToQtPy

// compilerClassName = CmmCustomCompiler
// cppClassName = ToCustomCpp
// pythonClassName = ToCustomPy

// enableMonitor = 1

// LIBS = "some-library"
// OBJECTS = [ "object1" object2 ]
// OBJECTS = object3
// SOURCES = [ abc.pas, def.cc ]

#if 0
   compilerClassName = CmmCompiler
   cppClassName = ToCpp
   pythonClassName = ToPy
#endif

#if 0
   compilerClassName = CmmInitCompiler
   cppClassName = ToInitCpp
   pythonClassName = ToInitPy
#endif

#if 0
   compilerClassName = CmmQtCompiler
   cppClassName = ToQtCpp
   pythonClassName = ToQtPy
#endif

#if 0
   title = "color-button"
   sourceFileNames = "../kit/qview/defs.h"
   sourceFileNames = "../kit/qview/edit.h"
   sourceFileNames = "../kit/qview/edit.cc"
   sourceFileNames = "../kit/qview/colorbutton.h"
   sourceFileNames = "../kit/qview/colorbutton.cc"
   sourceFileNames = "../kit/qview/toolbutton.h"
   sourceFileNames = "../kit/qview/toolbutton.cc"
   sourceFileNames = "../kit/qdemo/area.h"
   sourceFileNames = "../kit/qdemo/area.cc"
   sourceFileNames = "../kit/qdemo/builder.h"
   sourceFileNames = "../kit/qdemo/builder.cc"
   sourceFileNames = "../kit/qdemo/db.h"
   sourceFileNames = "../kit/qdemo/db.cc"
   sourceFileNames = "../kit/qdemo/dbus.h"
   sourceFileNames = "../kit/qdemo/dbus.cc"
   sourceFileNames = "examples/win.t"
   // compilerOptions = "-DQ_OBJECT -D slots -Dforeach(a,b)=for(a:b)"
   ignore_all_includes = true
#endif

// compile: /opt/cuda-10.2/bin/nvcc plain/cuda-mini.cu -o _output/cuda-mini.bin
// pacman -S cuda ( nvidia-470xx-dkms nvidia-470xx-utils )
// apt-get install nvidia-driver nvidia-cuda-toolkit

// compile: -lglui -lGL -lGLU -lglut -lstdc++ -lm
// dnf install glui-devel
// yay -S glui

// compile: -P gtkmm-3.0 -lstdc++ -fPIC
// dnf install gtkmm30-devel


#if 0
title = ""
icon = ""
shortcut = ""
tooltip = ""
toolbar = 0 # position in toolbar

cd = "" # current directory
set = [] # set environment variables
env = [] # environment names

cmd = "" # shell command
python_cmd = False # True => use Python executable as cmd
jscript = "" # java script
url = [] # web

module = "" # Python module
cls = "" # class
func = "" # function

load = [ ] # pre-load modules
loaded_module = "" # already loaded module

plugin = "" # parameters for builder from env.py
param = ""

enableMonitor = False
fast = False
ignore_all_includes = False
clang_import = False
compilerOptions = ""

compilerFileName = ""
compilerClassName = ""
compilerFuncName = ""
cppClassName = ""
pythonClassName = ""
outputSuffix = ""
sourceFileNames = [ ]

ext = "" # valid only for this file extension

addWin = False # add main window as parameter
addBuilder = False # add builder object as parameter
askFileName = "" # show file dialog
addFileName = False # add file name as parameter

TARGET = ""
QT = ""
CONFIG = ""

HEADERS = ""
PRECOMPILED_HEADER = ""
SOURCES = ""
FORMS = ""
RESOURCES = ""
SUBDIRS = ""

DEFINES = ""
INCLUDEPATH = ""
OBJECTS = ""
LIBS = ""
PKGCONFIG = ""
DISTFILES = ""
DESTDIR = ""

QMAKE_CC = ""
QMAKE_CXX = ""
QMAKE_EXT_H = ""
QMAKE_EXT_CC = ""
QMAKE_LINK = ""
QMAKE_CFLAGS = ""
QMAKE_CXXFLAGS = ""
QMAKE_LFLAGS = ""
QMAKE_CLEAN = ""
QMAKE_RPATHDIR = ""
#endif
}

/* ---------------------------------------------------------------------- */

#if 0
tools
{

Plugin
{
   title = EV3
   key = ev3
   module = "ev3/ev3_plugin"
   cls = EV3Plugin
   reread_menu = false
   reload_module = false
}

Menu "Tools"
MenuItem "QtCreator" { cmd = "qtcreator" }

Menu "&Help"
MenuItem "Qt"
{
   url="http://doc.qt.io/qt/classes.html"
}

Menu "C--"
MenuItem "win"
{
   param = example/win.t
   icon = "weather-clear"
   toolbar = 24
   fast = 1
}

/* Project menu - commands */

Command "configure"
{
   cd = "/abc/gibbon"
   set = "ABC = abc"
   set = "KLM = something another"
   cmd = "./autoinit"
}

Command build
{
   env = configure
   cmd = "./autobuild"
}

Command make
{
   cd = "/abc/gibbon"
   cmd = make
}

Command run
{
   cd = "/abc/gibbon"
   cmd = modules/appl/appl
}

Command clean
{
   cmd = "cd /abc/gibbon ; make clean"
}

Command install
{
   cmd = "cd /abc/gibbon ; make install"
}

Command "run-python"
{
   python_cmd = true // original Python executable
   // cmd="python2"
   // cmd = "/opt/python27/bin/python -B"
   addFileName = true
}

Command "compile-cpp"
{
   // cmd="gcc -O0 -g $"
   cmd="clang O0 -g $"
   addFileName = true
}

Command "run-cpp"
{
   // cmd="gcc -O0 -g $ -o _output/run.bin -B/usr/bin/mold && echo COMPILED && ./_output/run.bin"
   cmd="clang -w -O0 -g $ -o _output/run.bin -B/usr/bin/mold && echo COMPILED && ./_output/run.bin"
   // cmd="mkdir -p _output && clang -w -O0 -g $ -o _output/run.bin && echo COMPILED && ./_output/run.bin"

   addFileName = true
}

Command "debug-cpp"
{
   cmd="gcc -g $ -o _output/run.bin && echo COMPILED && kdbg ./_output/run.bin"
   addFileName = true
}

/* Option Dialog */

Cmd
{
   qmake = "qmake-q5"

   vcvars = "C:\\Appl\\MS\\VisualStudio2019\\VC\\Auxiliary\\Build\\vcvars64.bat"
   vcvars = "C:\\Appl\\MS\\VisualStudio2022\\VC\\Auxiliary\\Build\\vcvars64.bat"
   vcvars = "C:\\Appl\\MS\\Microsoft Visual Studio 2010\\VC\\bin\\amd64\\vcvars64.bat"
   vcvars = "C:\\Appl\\MS\\Microsoft Visual Studio 2008\\VC\\bin\\vcvars32.bat"

   nvcc = "/usr/local/cuda/bin/nvcc"
   nvcc = "/opt/cuda/bin/nvcc" // Arch Linux
   nvcc = "/opt/cuda-10.2/bin/nvcc"
   nvcc = "C:\\Appl\\Cuda\\Toolkit\\bin\\nvcc.exe"
}

/* Icon names and directories */

Icons
{
   path = "../../icons/qt4"
}

Icons
{
   path = "../../icons/monodevelop4"

   namespace = element-namespace-16
   class     = element-class-16
   function  = element-method-16
   variable  = element-field-16
   enum      = element-enumeration-16
}

Icons
{
   path = "/usr/share/app-info/icons/fedora/64x64"

   clementine = "clementine"
   choqok = "org.kde.choqok"
   tellico = "org.kde.tellico"
}

/* Color names */

Color
{
   name = "read"
   value =  "fuchsia"
}

Color
{
   name = "write"
   value = "blueviolet"
}

Color
{
   name = "signal"
   value = "plum"
}

/* Color tables */

ColorTable "classColors"
{
  "peru",
  "plum"
}

/* Additional Shortcuts and Icons */

Shortcut
{
   name = "Menu Edit / Clear Bookmarks"
   icon = stop
   shortcut = Alt+Shift+C
}

/* Toolbar */

Shortcut
{
   name = "Previous position"
   toolbar = -4
}

Shortcut
{
   name = "Next position"
   toolbar = -3
}

Shortcut
{
   name = "Menu View / Enlarge Font"
   toolbar = -2
}

Shortcut
{
   name = "Menu View / Shrink font"
   toolbar = -1
}

/* Module path */

PythonPath
{
   "/abc/llvm-python"
   "/abc/peachpy"
}

PythonPath
{
   "~/llvm-python",
   "/llvm-python",
}

/* Open files */

OpenFile "./examples/simple.ui";

} // end of tools
#endif

/* ---------------------------------------------------------------------- */

#if 0
python
{
def to_hex (n) :
    if n <= 9 :
       return chr (ord ('0') + n)
    else :
       return chr (ord ('a') + n - 10)

def read_hex_file () :
    fileName = QFileDialog.getOpenFileName (win, "Open File") [0]
    if fileName != "" :
       with open (fileName, "rb") as f :
          data = f.read ()

          text = ""
          cnt = 0
          for b in data :
             text += to_hex (b // 16) + to_hex (b % 16)
             cnt = cnt + 1
             if cnt < 16 :
                text += ' '
             else :
                text += '\n'
                cnt = 0

          clipboard = QApplication.clipboard ()
          clipboard.setText (text)

if 0 :
   from util import get_win
   win = get_win ()
   win.addMenuItem ("Tools", "read hex file", read_hex_file)
}
#endif

/* ---------------------------------------------------------------------- */

#if 0
file "icons/ellipse0.svg"
{
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22">
  <defs id="defs3051">
    <style type="text/css" id="current-color-scheme">
      .ColorScheme-Text {
        color:#4d4d4d;
      }
      </style>
  </defs>
 <path
    style="fill:currentColor;fill-opacity:1;stroke:none"
	d="M11 5A8 6 0 0 0 3 11 8 6 0 0 0 11 17 8 6 0 0 0 19 11 8 6 0 0 0 11 5M11 6A7 5 0 0 1 18 11 7 5 0 0 1 11 16 7 5 0 0 1 4 11 7 5 0 0 1 11 6"
    class="ColorScheme-Text"
    />
</svg>
}
#endif

// copy file "example/ready/icons/rectangle.svg" to "icons/rectangle.svg";

/*
hex_file "icons/line0.svg"
{
3c 73 76 67 20 78 6d 6c 6e 73 3d 22 68 74 74 70
3a 2f 2f 77 77 77 2e 77 33 2e 6f 72 67 2f 32 30
30 30 2f 73 76 67 22 20 76 69 65 77 42 6f 78 3d
22 30 20 30 20 32 32 20 32 32 22 3e 0d 0a 20 20
3c 64 65 66 73 20 69 64 3d 22 64 65 66 73 33 30
35 31 22 3e 0d 0a 20 20 20 20 3c 73 74 79 6c 65
20 74 79 70 65 3d 22 74 65 78 74 2f 63 73 73 22
20 69 64 3d 22 63 75 72 72 65 6e 74 2d 63 6f 6c
6f 72 2d 73 63 68 65 6d 65 22 3e 0d 0a 20 20 20
20 20 20 2e 43 6f 6c 6f 72 53 63 68 65 6d 65 2d
54 65 78 74 20 7b 0d 0a 20 20 20 20 20 20 20 20
63 6f 6c 6f 72 3a 23 34 64 34 64 34 64 3b 0d 0a
20 20 20 20 20 20 7d 0d 0a 20 20 20 20 20 20 3c
2f 73 74 79 6c 65 3e 0d 0a 20 20 3c 2f 64 65 66
73 3e 0d 0a 20 3c 70 61 74 68 20 0d 0a 20 20 20
20 73 74 79 6c 65 3d 22 66 69 6c 6c 3a 63 75 72
72 65 6e 74 43 6f 6c 6f 72 3b 66 69 6c 6c 2d 6f
70 61 63 69 74 79 3a 31 3b 73 74 72 6f 6b 65 3a
6e 6f 6e 65 22 20 0d 0a 09 64 3d 22 6d 31 36 20
33 76 32 2e 33 31 32 35 6c 2d 31 30 2e 36 38 37
35 20 31 30 2e 36 38 37 35 68 2d 32 2e 33 31 32
35 76 33 68 33 76 2d 32 2e 33 31 32 35 6c 31 30
2e 36 38 37 35 2d 31 30 2e 36 38 37 35 68 32 2e
33 31 32 35 76 2d 33 68 2d 33 6d 31 20 31 68 31
76 31 68 2d 31 76 2d 31 6d 2d 31 33 20 31 33 68
31 76 31 68 2d 31 76 2d 31 22 0d 0a 20 20 20 20
63 6c 61 73 73 3d 22 43 6f 6c 6f 72 53 63 68 65
6d 65 2d 54 65 78 74 22 0d 0a 20 20 20 20 2f 3e
20 20 0d 0a 3c 2f 73 76 67 3e 0d 0a
}
*/

/* ---------------------------------------------------------------------- */

#if 0
grammar
{
while_stat :  "while" "(" expr ")" stat   ;
if_stat    :  "if" "(" expr ")" stat ( "else" stat  )?   ;
compound_stat : "{" ( stat )* "}" ;
simple_stat :  expr ";" ;
empty_stat :  ";" ;
stat : while_stat | if_stat | compound_stat | simple_stat | empty_stat ;

simple_expr : identifier | number  | "(" expr ")" ;
mult_expr :  simple_expr ( ("*"|"/") simple_expr )* ;
add_expr :  mult_expr ( ("+"|"-") mult_expr )* ;
expr : add_expr ( "=" expr )? ;

program : stat;
}
#endif

/* ---------------------------------------------------------------------- */

// grammar "tutorial/cecko2.g";
// grammar "tutorial/cecko3.g";

/* ---------------------------------------------------------------------- */

/*
file "c2.cc"
{
   {
      n = 10;
      while ( n > 0 )
      {
         if ( sum )
            sum = sum + n;
         else
            sum = sum * n;
         n = n - 1;
      }
   }
}
*/

/* ---------------------------------------------------------------------- */

// program ;

#endif // IGNORE

/* ---------------------------------------------------------------------- */

#if 1
#define Q_OBJECT
#define slots
#define foreach(a,b) for (a : b)
// class ExtraSelection;
// class QTextEdit { class ExtraSelection; };
#endif

/* ---------------------------------------------------------------------- */

#define AREA
#define BUILDER
#define EDIT

CodeModule { code = "set/color-button.t"; }
CodeModule { code = "set/tool-button.t"; }
CodeModule { code = "set/info.t"; }
CodeModule { code = "set/grep.t"; }
CodeModule { code = "set/files.t"; }

#ifdef EDIT
   EditModule { }
#endif

#ifdef AREA
   // #include "set/area.t"
   CodeModule { code = "set/area.t"; }
#endif

#ifdef BUILDER
   // #include "set/builder.t"
   CodeModule { code = "set/builder.t"; }
#endif

// #include "set/construction.t"

/* ---------------------------------------------------------------------- */

// TraceModule { }

/* ---------------------------------------------------------------------- */

#if 0
struct TItem
{
    QString   text;
    bool      selected;
    int       size;
    double    value;
};

struct TData
{
   QList <TItem *> subitems;
};

JsonIO
{
    Record = TItem;
    Collection = TData;
    items = subitems;
}
#endif

/* ---------------------------------------------------------------------- */

struct Basic
{
};

struct Item : public Basic
{
    QString     name;
    bool        selected;
    int         size;
    double      value;
    #if 1
    QColor      color;
    QFont       font;
    QStringList list;
    #endif

    Item * up;
    QList <Item *> subitems;
};

struct Queue
{
    QList <Item *> subitems;
};


/* ---------------------------------------------------------------------- */

#if 0
QTableWidget * prop
{
}
#else
PropertyTableModule
{
   Record = Item;
   Properties = PropertyWindow;
}

PropertyWindow * prop
{
}
#endif

/* ---------------------------------------------------------------------- */

#if 0
PropertyTreeModule
{
   Record = Item;
   Properties = PropertyTreeWindow;
}
#endif

/* ---------------------------------------------------------------------- */

#if 0
TableModule
{
   Record = Item;

   Collection = Queue;
   items = subitems;

   store = storage;
}
#endif

/* ---------------------------------------------------------------------- */

#if 0
DialogModule
{
   Record = Item;
   Dialog = DialogWindow;
}
#endif

/* ---------------------------------------------------------------------- */

#if 0
DescriptionModule
{
   Record = Item;
   // Basic = QObject;
}
#endif

/* ---------------------------------------------------------------------- */

#if 1
JsonIO
{
    Record = Item;
    Collection = Queue;
    items = subitems;
}
#endif

/* ---------------------------------------------------------------------- */

#if 0
XmlIO
{
    Record = Item;
    Collection = Queue;
    items = subitems;
}
#endif

/* ---------------------------------------------------------------------- */

#if 0
TextIO
{
    Record = Item;
    Collection = Queue;
    items = subitems;
}
#endif

/* ---------------------------------------------------------------------- */

#if 0
FuncIO
{
    Record = Item;
}
#endif

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
   public:
      void readFile (QTextEdit * edit, QString fileName);
      QTextEdit * openEditor (QString fileName);

      void openFile ();
      void saveFile ();

      void findInFiles (); // before menu
      void runCommand (); // before menu

/* ---------------------------------------------------------------------- */

#if defined (AREA) || defined (BUILDER)
QToolBar * toolbar
{
    QToolButton * selectButton
    {
        text = "select";
        icon = "window-new";
    }

    QToolButton * resizeButton
    {
        text = "resize";
        icon = "view-refresh";
    }

    QToolButton * connectButton
    {
        text = "connect";
        icon = "go-jump";
    }

    QTabWidget * palette
    {
       QToolBar * colorToolBar
       {
          // text = "Colors";
          setTabText (0, "Colors");
          setTabIcon (0, QIcon::fromTheme ("color-fill"));

          new ColorButton (colorToolBar, "red");
          new ColorButton (colorToolBar, "blue");
          new ColorButton (colorToolBar, "green");
          new ColorButton (colorToolBar, "yellow");
          new ColorButton (colorToolBar, "orange");
          new ColorButton (colorToolBar, "silver");
          new ColorButton (colorToolBar, "gold");
          new ColorButton (colorToolBar, "goldenrod");
          new ColorButton (colorToolBar, "lime");
          new ColorButton (colorToolBar, "lime green");
          new ColorButton (colorToolBar, "yellow green");
          new ColorButton (colorToolBar, "green yellow");
          new ColorButton (colorToolBar, "forest green");
          new ColorButton (colorToolBar, "coral");
          new ColorButton (colorToolBar, "cornflower blue");
          new ColorButton (colorToolBar, "dodger blue");
          new ColorButton (colorToolBar, "royal blue");
          new ColorButton (colorToolBar, "wheat");
          new ColorButton (colorToolBar, "chocolate");
          new ColorButton (colorToolBar, "peru");
          new ColorButton (colorToolBar, "sienna");
          new ColorButton (colorToolBar, "brown");
       }

       QToolBar * shapeToolBar
       {
          // text = "Shapes";
          setTabText (0, "Shapes");
          new ToolButton (shapeToolBar, "area", "", shapeFormat);
          new ToolButton (shapeToolBar, "sub-area", "", shapeFormat);
          new ToolButton (shapeToolBar, "source", "", shapeFormat);
          new ToolButton (shapeToolBar, "target", "", shapeFormat);
          new ToolButton (shapeToolBar, "ellipse", "", shapeFormat);
       }

       #ifdef BUILDER
       QToolBar * componentToolBar
       {
          // text = "Components";
          setTabText (0, "Components");
          new ToolButton (componentToolBar, "QPushButton", "", widgetFormat);
          new ToolButton (componentToolBar, "QCheckBox", "", widgetFormat);
          new ToolButton (componentToolBar, "QComboBox", "", widgetFormat);
          new ToolButton (componentToolBar, "QLabel", "", widgetFormat);
          new ToolButton (componentToolBar, "QLineEdit", "", widgetFormat);
          new ToolButton (componentToolBar, "QPlainTextEdit", "", widgetFormat);
          new ToolButton (componentToolBar, "QTextEdit", "", widgetFormat);
          new ToolButton (componentToolBar, "QSpinBox", "", widgetFormat);
          new ToolButton (componentToolBar, "QDoubleSpinBox", "", widgetFormat);
          new ToolButton (componentToolBar, "QTreeWidget", "", widgetFormat);
          new ToolButton (componentToolBar, "QListWidget", "", widgetFormat);
          new ToolButton (componentToolBar, "QTableWidget", "", widgetFormat);
          new ToolButton (componentToolBar, "QTabWidget", "", widgetFormat);
          new ToolButton (componentToolBar, "QWidget", "", widgetFormat);
       }
       #endif
    }
}
#endif

/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */

QSplitter * vsplitter
{
   orientation = Qt::Vertical;
   QSplitter * hsplitter
   {
      QTabWidget * leftTabs
      {
          setTabPosition (QTabWidget::West);

          #if 0
          QTreeWidget * tree
          {
             QTreeWidgetItem * branch
             {
                QTreeWidgetItem * node1  { text = "red"; foreground = "red"; }
                QTreeWidgetItem * node2  { text = "green"; foreground = "green"; }
                QTreeWidgetItem * node3  { text = "blue"; foreground = "blue"; }

                text = "tree branch";
                foreground = "brown";
                background = "orange";
                icon = "folder";
             }
             expandAll ();
          }
          #else
          TreeModule
          {
             Record = Item;    // data class

             name = name;      // field with text
             above = up;       // field with pointer to above level
             items = subitems; // field with list of subitems

             root = storage;   // variable identifier for pointer to tree root
          }
          QTreeView * tree
          {
          }
          #endif

          GrepView * grep
          {
          }

          FileView * files
          {
          }
      }
      QTabWidget * middleTabs
      {
          #ifdef AREA
          AreaView * area
          {
          }
          #endif

          #ifdef BUILDER
          Builder * builder
          {
          }
          #endif

          #ifdef EDIT
          Edit * edit
          #else
          QTextEdit * edit
          #endif
          {
             int inx = middleTabs->indexOf (edit);
             middleTabs->setTabIcon (inx, QIcon::fromTheme ("window-close"));
             // setPlainText ("abc");
          }
      }
      QTabWidget * rightTabs
      {
          setTabPosition (QTabWidget::East);
          prop;
      }

      setStretchFactor (0, 1);
      setStretchFactor (1, 4);
      setStretchFactor (2, 1);
   }

   Info * info
   {
   }


   setStretchFactor (0, 3);
   setStretchFactor (1, 1);
}

#if 1
QStatusBar * status
{
    QLabel * lab
    {
       text = "status bar";
    }
}
#endif

/* ---------------------------------------------------------------------- */

// windowTitle = "Example";

QMenuBar * mainMenu
{
    QMenu * fileMenu
    {
        title = "&File";
        QAction * openMenuItem
        {
            text = "&Open...";
            shortcut = "Ctrl+O";
            triggered = openFile;
        }

        QAction * saveMenuItem
        {
            text = "&Save...";
            shortcut = "Ctrl+S";
            triggered { saveFile (); }
        }

        QAction * grepMenuItem
        {
            text = "&Search...";
            shortcut = "F2";
            // triggered = findInFiles;
            triggered { findInFiles (); }
        }

        QAction * runMenuItem
        {
            text = "&Run...";
            shortcut = "F5";
            triggered = runCommand;
        }

        QAction * quitMenuItem
        {
            text = "&Quit";
            shortcut = "Ctrl+Q";
            triggered { close (); }
        }
    }
}

/* ---------------------------------------------------------------------- */

   // class Window
   public:
      Item * top;
      void setup ();

      Window (QWidget * parent = null) :
         QMainWindow (parent)
      {
          setup ();
          tree.setModel (new TreeModel (this, top));
      }
};

/* ---------------------------------------------------------------------- */

void Window::setup ()
{
   top = new Item;
   top->name = "top";

   Item * item1
   {
      name = "abc";
      selected = true;
      size = 7;
      value = 3.14;
      color = QColor ("orange");
      list << "abc" << "def" << "klm";
   }
   top->subitems.append (item1);

   Item * item2
   {
      name = "def";
   }
   top->subitems.append (item2);
}

/* ---------------------------------------------------------------------- */

void Window::readFile (QTextEdit * edit, QString fileName)
{
   QFile f (fileName);
   if (f.open (QFile::ReadOnly))
   {
      QByteArray code = f.readAll ();
      edit->setPlainText (code);
   }
   else
   {
      QMessageBox::warning (null, "Open File Error", "Cannot read file: " + fileName);
   }
}

QTextEdit * Window::openEditor (QString fileName)
{
   QTextEdit * edit = null;
   QFileInfo fi (fileName);
   fileName = fi.absoluteFilePath();
   /*
   if (sourceEditors.contains (fileName))
   {
      edit = sourceEditors [fileName];
   }
   else
   {
   }
   */
   #ifdef EDIT
      edit = new Edit (this);
   #else
      edit = new QTextEdit (this);
   #endif
   edit->setToolTip (fileName);
   middleTabs->addTab (edit, fi.fileName ());
   // sourceEditors [fileName] = edit;
   readFile (edit, fileName);

   return edit;
}

void Window::openFile ()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open text file");
    if (fileName != "")
    {
       openEditor (fileName);
    }
}

void Window::saveFile ()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save text file");
    if (fileName != "")
    {
    }
}

void Window::findInFiles ()
{
    new GrepDialog (grep);
}

void Window::runCommand ()
{
    info->runCommand ("gcc -c _output/win_output.cpp `pkg-config Qt5Widgets --cflags`");
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
   // cout << "Hello"; //  << "Qt";
   // QIcon::setThemeName ("oxygen");

   QApplication * appl = new QApplication (argc, argv);
   Window * win = new Window ();
   win->show ();
   appl->exec();
}

#endif

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
