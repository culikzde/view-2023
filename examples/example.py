#!/usr/bin/env python

from __future__ import print_function

def hello () :
   print ("Hello from function")

print ("Hello from main program ... ")

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
