#ifndef SIMPLE_H
#define SIMPLE_H

#include <QWidget>
#include <QListWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Simple; }
QT_END_NAMESPACE

class Simple : public QWidget
{
    Q_OBJECT

public:
    Simple(QWidget *parent = nullptr);
    ~Simple();

private:
    Ui::Simple *ui;
};
#endif // SIMPLE_H
