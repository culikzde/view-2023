#include "simple.h"
#include "ui_simple.h"
#include <QApplication>

Simple::Simple(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Simple)
{
    ui->setupUi(this);
}

Simple::~Simple()
{
    delete ui;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Simple w;
    w.show();
    return a.exec();
}


