#!/usr/bin/env python

import sys

from util import use_pyside2, use_pyqt5
if use_pyside2 :
   from PySide2.QtCore import *
   from PySide2.QtGui import *
   from PySide2.QtWidgets import *
   from PySide2.QtSql import *
elif use_pyqt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
   from PyQt5.QtSql import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *
   from PyQt4.QtSql import *

# --------------------------------------------------------------------------

class DatabaseInfo (object) :
   def __init__ (self) :
       self.item_list = [ ]

class TableInfo (object) :
   def __init__ (self) :
       self.item_name = ""
       self.item_list = [ ]

class ColumnInfo (object) :
   def __init__ (self) :
       self.item_name = ""

# --------------------------------------------------------------------------

class DbView (QWidget):

   def __init__ (self, win) :
       super (DbView, self).__init__ (win)
       self.win = win

       layout = QVBoxLayout ()
       self.setLayout (layout)

       table = QTableView ()
       layout.addWidget (table)

       db = QSqlDatabase.addDatabase ("QSQLITE")
       # db.setDatabaseName (":memory:")
       db.setDatabaseName ("examples/test.sqlite")
       db.open ()

       query = db.exec_ ("CREATE TABLE IF NOT EXISTS colors (name TEXT, red INTEGER, green INTEGER, blue INTEGER)")

       db.exec_ ("INSERT INTO colors (name, red, green, blue) VALUES (\"blue\", 0, 0, 255)")

       insert = QSqlQuery (db)
       insert.prepare ("INSERT INTO colors (name, red, green, blue) VALUES (:name, :red, :green, :blue)")
       insert.bindValue (":name", "green")
       insert.bindValue (":red", QVariant (0))
       insert.bindValue (":green", QVariant (255))
       insert.bindValue (":blue", QVariant (0))
       insert.exec_ ()

       model = QSqlTableModel (self, db)
       model.setTable ("colors")
       model.select ()

       table.setModel (model)

       database_info = DatabaseInfo ()
       database_info.item_name = db.databaseName()
       database_info.item_expand = True

       table_list = db.tables()
       for table_name in table_list :
           query = db.exec_ ("SELECT * FROM " + table_name)

           table_info = TableInfo ()
           table_info.item_name = table_name
           table_info.item_expand = True
           database_info.item_list.append (table_info)

           rec = query.record ()
           for k in range (rec.count ()) :
              txt = rec.fieldName (k)
              column_info = ColumnInfo ()
              column_info.item_name = txt;
              table_info.item_list.append (column_info)

       self.win.addView ("Database", self)
       self.win.addNavigatorData (self, database_info)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
