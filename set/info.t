
/* info.t */

/* ---------------------------------------------------------------------- */

class Info: public QPlainTextEdit
{
   public:
      Info (QWidget * p_win);
      void runCommand (QString cmd);

   private :
      void gccLine (QTextCursor cursor, QByteArray line);

      void modifyLine (QTextCursor cursor, QString line, QString keyword);
      void directoryLine (QTextCursor cursor, QString line);
      void pythonLine (QTextCursor cursor, QString line);

      void setColor (QTextCursor & cursor, QColor color);
      void setNote (QTextCursor & cursor, QString note);
      void clearNote (QTextCursor & cursor);

      void commandOneLine (QByteArray line);

      void commandDataReady ();
      void commandDataFinished ();

      /*
      void mousePressEvent (QMouseEvent * e);
      void getLocation (QString mark);

      void goToLocation (QString fileName, int line, int column);
      void goToKDevelop (QString fileName, int line, int column);
      void jumpToPrevMark ();
      void jumpToNextMark ();

      void jumpToFirstMark ();
      void jumpToLastMark ();
      void scrollUp ();
      void scrollDown ();
      */

      QWidget * win;

      QColor red, green, blue, gray, norm, brown, orange, yellow, cornflowerblue;

      QProcess process;
      QString directory;
      QByteArray buffer;
};

/* ---------------------------------------------------------------------- */

Info::Info (QWidget * p_win) :
    QPlainTextEdit (p_win),
    win (p_win)
{
    setLineWrapMode (QPlainTextEdit::NoWrap);
    directory = "";
    buffer = "";

    red = QColor ("red");
    green = QColor ("green");
    blue = QColor ("blue");
    gray = QColor ("gray");
    norm = QColor ("black");
    brown = QColor ("brown");
    orange = QColor ("orange");
    yellow = QColor ("yellow");
    cornflowerblue = QColor ("cornflowerblue");
}

void Info::setColor (QTextCursor & cursor, QColor color)
{
   QTextCharFormat format = cursor.charFormat ();
   format.setForeground (color);
   cursor.setCharFormat (format);
}

void Info::setNote (QTextCursor & cursor, QString note)
{
   QTextCharFormat format = cursor.charFormat ();
   format.setProperty (QTextFormat::UserProperty, note); // !?
   cursor.setCharFormat (format);
}

void Info::clearNote (QTextCursor & cursor)
{
   QTextCharFormat format = cursor.charFormat ();
   format.clearProperty (QTextFormat::UserProperty); // !?
   cursor.setCharFormat (format);
}

void Info::gccLine (QTextCursor cursor, QByteArray line)
{
    bool cont = false;
    for (QByteArray word : line.split (' '))
    {
        bool style = false;
        if (!cont)
        {
            if ((word.endsWith (".c") || word.endsWith (".cc") || word.endsWith (".cpp")))
            {
                setColor (cursor, brown);
                style = true;
            }
        }
        if (!cont)
        {
            if (word.startsWith ("-I"))
            {
                setColor (cursor, green);
                style = true;
            }
            else if ((word.startsWith ("-D") || word.startsWith ("-U")))
            {
                setColor (cursor, orange);
                style = true;
            }
            else if (word.startsWith ("-L"))
            {
                setColor (cursor, cornflowerblue);
                style = true;
            }
            else if (word.startsWith ("-l"))
            {
                setColor (cursor, blue);
                style = true;
            }
            else if (word.startsWith ("-o"))
            {
                setColor (cursor, red);
                style = true;
            }
        }

        bool next_cont = (style && word.length () == 2);
        cursor.insertText (word);

        if (style && ! next_cont || cont)
            setColor(cursor, norm);
            cursor.insertText (" "); // necessary to apply normal color

        cont = next_cont;
    }
}

void Info::modifyLine (QTextCursor cursor, QString line, QString keyword)
{
   QString pattern = "(.*\\s)?(\\S*):(\\d\\d*):(\\d\\d*):(\\s*" + keyword + ":)(.*)";
   QRegExp re (pattern);
   int pos = re.indexIn (line);
   QStringList m = re.capturedTexts ();
   if (pos >= 0)
   {
       QString mark = directory + QDir::separator () + m[2] + ':' + m[3] + ':' + m[4];
       setNote (cursor, mark);

       if (m[1] != "")
          cursor.insertText (m[1]);
       setColor (cursor, blue);

       cursor.insertText (m[2]);
       setColor (cursor, norm);

       cursor.insertText (":");
       setColor (cursor, green);

       cursor.insertText (m[3]);
       setColor (cursor, norm);

       cursor.insertText (":");
       setColor (cursor, gray);

       cursor.insertText (m[4]);
       setColor (cursor, red);

       cursor.insertText (m[5]);
       setColor (cursor, orange);

       cursor.insertText (m[6]);
       setColor (cursor, norm);

       clearNote (cursor);
   }
   else
   {
       setColor (cursor, brown);
       cursor.insertText (line);
       setColor (cursor, norm);
   }
}

void Info::directoryLine (QTextCursor cursor, QString line)
{
   QString pattern = "Entering directory '([^']*)'";
   QRegExp re (pattern);
   int pos = re.indexIn (line);
   QStringList m = re.capturedTexts ();

   if (pos >= 0)
   {
       directory = m[0];
       setColor (cursor, orange);
   }
   else
   {
       setColor (cursor, brown);
   }
   cursor.insertText (line);
   setColor (cursor, norm);
}

void Info::pythonLine (QTextCursor cursor, QString line)
{
   QString pattern = "  File (.*), line ([0-9]*)(, in (.*))?";
   QRegExp re (pattern);
   int pos = re.indexIn (line);
   QStringList m = re.capturedTexts ();
   if (pos >= 0)
   {
       QString fileName = m[1];
       QString line = m[2];

       if ((fileName.startsWith ('"') && fileName.endsWith ('"')))
           // fileName = fileName[1:(-1)];
           fileName = fileName.mid (1, fileName.length()-2);

       QString mark = fileName + ':' + line;
       setNote (cursor, mark);

       cursor.insertText ("File ");
       setColor (cursor, blue);

       cursor.insertText (m[1]);
       setColor (cursor, norm);

       cursor.insertText (", line ");
       setColor (cursor, green);

       cursor.insertText (m[2]);
       setColor (cursor, norm);

       if (m[3] != null)
       {
           cursor.insertText (", in ");
           setColor (cursor, orange);

           cursor.insertText (m[3]);
           setColor (cursor, norm);
       }

       clearNote (cursor);
   }
   else
   {
       setColor (cursor, brown);
       cursor.insertText (line);
       setColor (cursor, norm);
   }
}

void Info::commandOneLine (QByteArray line)
{
    QTextCursor cursor = textCursor ();
    cursor.movePosition (QTextCursor::End);

    if (line.indexOf ("fatal error:") >= 0)
        modifyLine (cursor, line, "fatal error");
    else if (line.indexOf ("error:") >= 0)
        modifyLine (cursor, line, "error");
    else if (line.indexOf ("warning:") >= 0)
        modifyLine (cursor, line, "warning");
    else if (line.indexOf ("debug:") >= 0)
        modifyLine (cursor, line, "debug");
    else if (line.indexOf ("info:") >= 0)
        modifyLine (cursor, line, "info");
    else if (line.indexOf ("Entering directory ") >= 0)
        directoryLine (cursor, line);
    else if (line.indexOf ("Leaving directory ") >= 0)
    {
        directory = "";
        cursor.insertText (line);
    }
    else if (line.startsWith ("  File "))
        pythonLine (cursor, line);
    else if ((line.indexOf ("gcc") >= 0 || line.indexOf ("g++") >= 0))
        gccLine (cursor, line);
    else
        cursor.insertText (line);

    cursor.insertText ("\n");
}

void Info::commandDataReady ()
{
    QByteArray data = process.readAll ();
    for (char c : data)
    {
        if (c == '\n')
        {
            commandOneLine (buffer);
            buffer = "";
        }
        else
            buffer += c;
    }
}

void Info::commandDataFinished ()
{
    if (buffer != "")
    {
        commandOneLine (buffer);
        buffer = "";
    }
    ensureCursorVisible ();
}

void Info::runCommand (QString cmd)
{
   // process = new QProcess (win);

   process.setProcessChannelMode (QProcess::MergedChannels);
   process { readyRead = commandDataReady; }
   // process.readyRead { commandDataReady (); }
   // process.readyRead = commandDataReady;
   process { finished = commandDataFinished; }

   commandOneLine ("RUN " + cmd.toLatin1());
   QStringList args;
   args << "-c" << cmd;
   process.start ("/bin/sh", args);
}

#if 0
void Info::mousePressEvent (QMouseEvent * e)
{
    cursor = cursorForPosition (mouse_event_pos (e));
    cursor.select (QTextCursor.WordUnderCursor);
    format = cursor.charFormat ();
    mark = str (format.stringProperty (Text.locationProperty));
    if (mark != "")
    {
        (fileName, line, column) = getLocation (mark);
        if (fileName != null)
        {
            modifiers = e.modifiers ();
            if (modifiers & Qt.MetaModifier)
            {
                goToLocation (fileName, line, column);
                goToKDevelop (fileName, line, column);
            }
            else
                goToLocation (fileName, line, column);
        }
    }
    super (Info, this).mousePressEvent (e);
}

void Info::getLocation (QString mark)
{
    pattern = "([^:]*)(:([0-9]*)(:([0-9]*))?)?";
    m = re.match (pattern, mark);
    if (m)
    {
        fileName = m.group (1);
        line = m.group (3);
        column = m.group (5);
        if ((line == null || line == ""))
            line = 0;
        else
            line = int (line);
        if ((column == null || column == ""))
            column = 0;
        else
            column = int (column);
    }
    else
    {
        fileName = null;
        line = 0;
        column = 0;
    }
    return (fileName, line, column);
}

void Info::goToLocation (QString fileName, int line, int column)
{
   if (win != null)
       win.loadFile (fileName, line, column);
}

void Info::goToKDevelop (QString fileName, int line, int column)
{
}

void Info::jumpToPrevMark ()
{
   cursor = textCursor ();
   cursor.movePosition (QTextCursor.StartOfLine);
   stop = false;
   found = false;
   while (((!stop) && (!found)))
   {
       if ((!cursor.movePosition (QTextCursor.Up)))
           stop = true;
       if (cursor.charFormat().hasProperty (Text.locationProperty))
           found = true;
       if (cursor.atStart())
           stop = true;
   }
   if (found)
       cursor.movePosition (QTextCursor.EndOfLine, QTextCursor.KeepAnchor);
   setTextCursor (cursor);
   ensureCursorVisible ();
   if (found)
   {
       mark = str (cursor.charFormat ().stringProperty (Text.locationProperty));
       (fileName, line, column) = getLocation (mark);
       goToLocation (fileName, line, column);
       win.showStatus ("");
   }
   else
       win.showStatus ("Begin of file");
}

void Info::jumpToNextMark (self)
{
   cursor = textCursor ();
   cursor.movePosition (QTextCursor.StartOfLine);
   stop = false;
   found = false;
   while (((!stop) && (!found)))
   {
       if ((!cursor.movePosition (QTextCursor.NextBlock)))
           stop = true;
       if (cursor.charFormat ().hasProperty (Text.locationProperty))
           found = true;
       if (cursor.atEnd ())
           stop = true;
   }
   if (found)
       cursor.movePosition (QTextCursor.EndOfLine, QTextCursor.KeepAnchor);
   setTextCursor (cursor);
   ensureCursorVisible ();
   if (found)
   {
       mark = str (cursor.charFormat ().stringProperty (Text.locationProperty));
       (fileName, line, column) = getLocation (mark);
       goToLocation (fileName, line, column);
       win.showStatus ("");
   }
   else
       win.showStatus ("End of file");
}

void Info::jumpToFirstMark ()
{
   cursor = textCursor ();
   cursor.movePosition (QTextCursor.Start);
   setTextCursor (cursor);
   jumpToNextMark ();
}

void Info::jumpToLastMark ()
{
   cursor = textCursor ();
   cursor.movePosition (QTextCursor.End);
   setTextCursor (cursor);
   jumpToPrevMark ();
}

void Info::scrollUp ()
{
    verticalScrollBar ().triggerAction (QAbstractSlider.SliderSingleStepSub);
}

void Info::scrollDown ()
{
    verticalScrollBar ().triggerAction (QAbstractSlider.SliderSingleStepAdd);
}
#endif

/* ---------------------------------------------------------------------- */
