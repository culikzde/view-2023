
class Line : public QGraphicsLineItem
{
    int x1, y1, x2, y2;
    QColor pen;
};

class Rectangle : public QGraphicsRectItem
{
    QString name;
    int x, y, width, height;
    QColor pen;
    QColor brush;
};

class Ellipse : public QGraphicsEllipseItem
{
    int x, y, width, height;
    QColor pen;
    QColor brush;
};

class Shape : public QAbstractGraphicsShapeItem
{
    property double x
    {
        double get ()
        {
           return pos().x();
        }

        void set (double value)
        {
           QPointF point = pos ();
           point.setX (value);
           setPos (point);
        }
    }

    property double y
    {
        get ()
        {
           return pos().y();
        }

        set ()
        {
           QPointF point = pos ();
           point.setY (value);
           setPos (point);
        }
    }

    property double width
    {
        get
        {
           return rect().width();
        }

        set
        {
            QRectF box = rect ();
            box.setWidth (value);
            setRect (box);
        }
    }

    property double height
    {
        get
        {
           return this->rect().height();
        }

        set
        {
            QRectF box = this->rect ();
            box.setHeight (value);
            this->setRect (box);
        }
    }

    property QColor color
    {
       get { return pen ().color (); }

       set
       {
           QPen p = pen ();
           p.setColor (value);
           setPen (p);
       }
    }

    property QColor border
    {
       get { return brush ().color (); }

       set
       {
           QBrush b = brush ();
           b.setColor (value);
           setBrush (b);
       }
   }
};


// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
