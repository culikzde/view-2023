/* builder-example.t */

#include "tool-button.t"
#include "builder.t"

/* ---------------------------------------------------------------------- */

QMenuBar * mainMenu
{
    QMenu * fileMenu
    {
        title = "&File";
        QAction * openMenuItem
        {
            text = "&Open...";
            shortcut = "Ctrl+O";
            triggered = openFile;
        }

        QAction * saveMenuItem
        {
            text = "&Save...";
            shortcut = "Ctrl+S";
            triggered { saveFile (); }
        }

        QAction * quitMenuItem
        {
            text = "&Quit";
            shortcut = "Ctrl+Q";
            triggered { this->close (); }
        }
    }
}

/* ---------------------------------------------------------------------- */

QTabWidget * toolbar
{
   QToolBar * componentToolBar
   {
      text = "Components";
      new ToolButton (componentToolBar, "QPushButton", "", widgetFormat);
      new ToolButton (componentToolBar, "QCheckBox", "", widgetFormat);
      new ToolButton (componentToolBar, "QComboBox", "", widgetFormat);
      new ToolButton (componentToolBar, "QLabel", "", widgetFormat);
      new ToolButton (componentToolBar, "QLineEdit", "", widgetFormat);
      new ToolButton (componentToolBar, "QPlainTextEdit", "", widgetFormat);
      new ToolButton (componentToolBar, "QTextEdit", "", widgetFormat);
      new ToolButton (componentToolBar, "QSpinBox", "", widgetFormat);
      new ToolButton (componentToolBar, "QDoubleSpinBox", "", widgetFormat);
      new ToolButton (componentToolBar, "QTreeWidget", "", widgetFormat);
      new ToolButton (componentToolBar, "QListWidget", "", widgetFormat);
      new ToolButton (componentToolBar, "QTableWidget", "", widgetFormat);
      new ToolButton (componentToolBar, "QTabWidget", "", widgetFormat);
      new ToolButton (componentToolBar, "QWidget", "", widgetFormat);
   }
}

/* ---------------------------------------------------------------------- */

QStatusBar * status
{
    QLabel * lab
    {
        text = "status bar";
    }
}

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
   windowTitle = "Builder Example";

   mainMenu;
   toolbar;

   QSplitter * vsplitter
   {
       orientation = Qt::Vertical;
       QSplitter * hsplitter
       {
          QTreeWidget * tree { }
          Builder * builder { }
          QTableWidget * prop { }

          setStretchFactor (0, 1);
          setStretchFactor (1, 4);
          setStretchFactor (2, 1);
       }
       QPlainTextEdit * info { }
       setStretchFactor (0, 3);
       setStretchFactor (1, 1);
   }

   status;

public:
   void openFile ();
   void saveFile ();
};

/* ---------------------------------------------------------------------- */

void Window::openFile ()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open text file");
    if (fileName != "")
    {
    }
}

void Window::saveFile ()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save text file");
    if (fileName != "")
    {
    }
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
   // QIcon::setThemeName ("oxygen");
   QApplication * appl = new QApplication (argc, argv);
   Window * win = new Window ();
   win->show ();
   appl->exec ();
}


// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
