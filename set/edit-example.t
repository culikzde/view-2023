/* edit-example.t */

EditModule { }

/* ---------------------------------------------------------------------- */

class EditWin : public QMainWindow
{
   private:
      QVBoxLayout * vlayout
      {
         Edit * edit { }
         FindBox * findBox { hide (); }
         GoToLineBox * lineBox { hide (); }

         edit->findBox = findBox;
         edit->lineBox = lineBox;
      }

  private:
      void readFile (QString fileName);
      void writeFile (QString fileName);

   public:
      void openFile ();
      void saveFile ();

   public:

   QMenuBar * mainMenu
   {
       QMenu * fileMenu
       {
           title = "&File";

           QAction * openMenuItem
           {
              text = "&Open...";
              shortcut = "Ctrl+O";
              triggered () { openFile (); }
           }

           QAction * saveMenuItem
           {
              text  = "Save &As...";
              shortcut = "Ctrl+S";
              triggered () { saveFile (); }
           }

           QAction * quitMenuItem
           {
               text = "&Quit";
               shortcut = "Ctrl+Q";
               triggered () { this->close (); }
           }
       }
       QMenu * editMenu
       {
           title = "&Edit";

           QAction * indentMenuItem
           {
              text = "&Indent";
              shortcut = "Ctrl+I";
              triggered { edit->indent (); }
           }

           QAction * unindentMenuItem
           {
              text = "&Unindent";
              shortcut = "Ctrl+U";
              triggered { edit->unindent (); }
           }

           QAction * sep1
           {
              separator = true;
           }

           QAction * commentMenuItem
           {
              text = "&Comment";
              shortcut = "Ctrl+D";
              triggered { edit->comment (); }
           }

           QAction * uncommentMenuItem
           {
              text = "Uncomment";
              shortcut = "Ctrl+E";
              triggered { edit->uncomment (); }
           }

           QAction * sep2
           {
              separator = true;
           }

           QAction * nextFunctionMenuItem
           {
              text = "Move Lines &Up";
              shortcut = "Ctrl+Shift+Up";
              triggered { edit->moveLinesUp (); }
           }

           QAction * moveLinesDownMenuItem
           {
              text = "Move Lines &Down";
              shortcut = "Ctrl+Shift+Down";
              triggered { edit->moveLinesDown (); }
           }

       }
       QMenu * viewMenu
       {
           title = "&View";

           QAction * enlargeFontMenuItem
           {
              text = "&Enlarge Font";
              shortcut = "Ctrl+=";
              triggered { edit->enlargeFont (); }
           }

           QAction * shrinkFontMenuItem
           {
              text = "&Shrink Font";
              shortcut = "Ctrl+-";
              triggered { edit->shrinkFont (); }
           }
       }
       QMenu * searchMenu
       {
           title = "&Search";

           QAction * findMenuItem
           {
              text = "&Find ..";
              shortcut = "Ctrl+F";
              triggered { edit->findText (); }
           }

           QAction * replaceMenuItem
           {
              text = "&Replace ...";
              shortcut = "Ctrl+R";
              triggered { edit->replaceText (); }
           }

           QAction * findNextMenuItem
           {
              text = "Find &Next";
              shortcut = "F3";
              triggered { edit->findNext (); }
           }

           QAction * findPrevMenuItem
           {
              text = "Find Pre&vious";
              shortcut = "Shift+F3";
              triggered { edit->findPrev (); }
           }

           QAction * gotoLineMenuItem
           {
              text = "Go to &Line";
              shortcut = "Ctrl+G";
              triggered { edit->goToLine (); }
           }
       }
       QMenu * bookmarkMenu
       {
           title = "&Bookmark";

           QAction * setBookmarkMenuItem
           {
              text = "Set &Bookmark";
              shortcut = "Ctrl+B";
              triggered { edit->setBookmark (0); }
           }

           QAction * goToPrevBookmarkMenuItem
           {
              text = "Go to &Previous Bookmark";
              shortcut = "Ctrl+N";
              triggered { edit->gotoPrevBookmark (0); }
           }

           QAction * goToNextBookmarkMenuItem
           {
              text = "Go to &ext Bookmark";
              shortcut = "Ctrl+M";
              triggered { edit->gotoNextBookmark (0); }
           }

           QAction * clearBookmarksMenuItem
           {
              text = "&Clear All Bookmarks";
              triggered { edit->clearBookmarks (); }
           }
       }
   }
};

/* ---------------------------------------------------------------------- */

void EditWin::readFile (QString fileName)
{
   QFile file (fileName);
   if (file.open (QIODevice::ReadOnly | QIODevice::Text))
   {
       QByteArray data = file.readAll ();
       QTextCodec * codec = QTextCodec::codecForName ("UTF-8");
       QString text = codec->toUnicode (data);
       edit->setPlainText (text);
   }
}

void EditWin::writeFile (QString fileName)
{
   QFile file (fileName);
   if (file.open (QIODevice::ReadWrite | QIODevice::Text))
   {
       QTextCodec * codec = QTextCodec::codecForName ("UTF-8");
       QString text = edit->toPlainText ();
       QByteArray data = codec->fromUnicode (text);
       file.write (data);
   }
}

void EditWin::openFile ()
{
   QString fileName = QFileDialog::getOpenFileName (this, "Open file");
   if (fileName != "")
      readFile (fileName);
}

void EditWin::saveFile ()
{
   QString fileName = QFileDialog::getSaveFileName (this, "Save file");
   if (fileName != "")
      writeFile (fileName);
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
   QApplication * appl = new QApplication (argc, argv);
   // QIcon::setThemeName ("oxygen");

   EditWin * win = new EditWin ();
   win->show ();

   appl->exec();
}

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
