
/* prop-table-example.t */

/* ---------------------------------------------------------------------- */

struct Basic
{
    QString   name;
};

struct Item : public Basic
{
    bool        selected;
    int         size;
    double      value;
    QColor      color;
    QFont       font;
    QStringList list;
};

/* ---------------------------------------------------------------------- */

PropertyTableModule
{
   Record = Item;
   Properties = PropertyWindow;
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
     QApplication * appl = new QApplication (argc, argv);

     Item * item = new Item;
     item->name = "abc";
     item->selected = true;
     item->size = 7;
     item->value = 3.14;
     item->color = QColor ("orange");
     item->list << "abc" << "def" << "klm";

     PropertyWindow * window = new PropertyWindow (NULL, item);
     appl->exec ();
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
