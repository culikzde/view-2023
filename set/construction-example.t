/* builder-example.t */

#include "tool-button.t"
#include "construction.t"

/* ---------------------------------------------------------------------- */

QMenuBar * mainMenu
{
    QMenu * fileMenu
    {
        title = "&File";
        QAction * openMenuItem
        {
            text = "&Open...";
            shortcut = "Ctrl+O";
            triggered = openFile;
        }

        QAction * saveMenuItem
        {
            text = "&Save...";
            shortcut = "Ctrl+S";
            triggered { saveFile (); }
        }

        QAction * quitMenuItem
        {
            text = "&Quit";
            shortcut = "Ctrl+Q";
            triggered { this->close (); }
        }
    }
}

/* ---------------------------------------------------------------------- */

QTabWidget * toolbar
{
   QToolBar * componentToolBar
   {
      text = "Components";
      new ToolButton (componentToolBar, "Sphere", "", ahapeFormat);
      new ToolButton (componentToolBar, "Box", "", shapeFormat);
   }
}

/* ---------------------------------------------------------------------- */

QSplitter * vsplitter
{
    orientation = Qt::Vertical;
    QSplitter * hsplitter
    {
       QTreeWidget * tree { }
       Builder * builder { }
       QTableWidget * prop { }

       setStretchFactor (0, 1);
       setStretchFactor (1, 4);
       setStretchFactor (2, 1);
    }
    QPlainTextEdit * info { }
    setStretchFactor (0, 3);
    setStretchFactor (1, 1);
}

/* ---------------------------------------------------------------------- */

QStatusBar * status
{
    QLabel * lab
    {
        text = "status bar";
    }
}

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
   windowTitle = "Qt 3D Example";

   mainMenu;
   toolbar;
   vsplitter;
   status;

public:
   void openFile ();
   void saveFile ();
};

/* ---------------------------------------------------------------------- */

void Window::openFile ()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open text file");
    if (fileName != "")
    {
    }
}

void Window::saveFile ()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save text file");
    if (fileName != "")
    {
    }
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
   // QIcon::setThemeName ("oxygen");
   QApplication * appl = new QApplication (argc, argv);
   Window * win = new Window ();
   win->show ();
   appl->exec ();
}


// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
