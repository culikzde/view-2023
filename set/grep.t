/* grep.t */

/* ---------------------------------------------------------------------- */

class TreeItem : public QTreeWidgetItem
{
   public:
       QString fileName;
       int src_line;

       TreeItem (QTreeWidget * tree) : QTreeWidgetItem (tree) { }
       TreeItem (QTreeWidgetItem * branch) : QTreeWidgetItem (branch) { }
};

/* ---------------------------------------------------------------------- */

class GrepView: public QWidget
{
   public:
      GrepView (QWidget * p_win);

      void prevItem ();
      void nextItem ();
      void grep (QString params);

   private:
      QWidget * win;

      QTreeWidget * tree;
      QPushButton * stopButton;

      QProcess * process;

      QString lastFileName;
      TreeItem * branch = null;

      bool startLine;
      bool middle;
      QString fileName;
      int lineNum;
      QString text;

      void initVariables ();
      void initLineVariables ();

      void showItem ();
      void dataReady ();
      void stopProcess ();
      void onItemActivated (QTreeWidgetItem * node, int column);
};

/* ---------------------------------------------------------------------- */

class GrepDialog: public QDialog
{
   public:
      GrepDialog (GrepView * p_win);
      // void openDialog (QTextEdit * editor);

   private:
      GrepView * win;

      QLineEdit * pattern;
      QCheckBox * caseSensitive;
      QCheckBox * wholeWords;
      QCheckBox * regularExpression;
      QComboBox * directory;

      void search ();
};

/* ---------------------------------------------------------------------- */

GrepDialog::GrepDialog (GrepView * p_win) :
   QDialog (p_win),
   win (p_win)
{
   setWindowTitle ("Find in Files");

   QGridLayout * layout = new QGridLayout (this);
   setLayout (layout);

   QLabel * label1 = new QLabel ("Find:", this);
   layout->addWidget (label1, 0, 0);

   pattern = new QLineEdit (this);
   layout->addWidget (pattern, 0, 1);

   QLabel * label2 = new QLabel ("Case Sensitive:", this);
   layout->addWidget (label2, 1, 0);

   caseSensitive = new QCheckBox (this);
   layout->addWidget (caseSensitive, 1, 1);

   QLabel * label3 = new QLabel ("Whole words:", this);
   layout->addWidget (label3, 2, 0);

   wholeWords = new QCheckBox (this);
   layout->addWidget (wholeWords, 2, 1);

   QLabel * label4 = new QLabel ("Regular expression:", this);
   layout->addWidget (label4, 3, 0);

   regularExpression = new QCheckBox (this);
   layout->addWidget (regularExpression, 3, 1);

   QLabel * label5 = new QLabel ("Directory:", this);
   layout->addWidget (label5, 4, 0);

   directory = new QComboBox (this);
   directory->setEditable (true);
   directory->addItem (".");
   directory->addItem ("..");
   layout->addWidget (directory, 4, 1);

   QDialogButtonBox * box = new QDialogButtonBox (QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
   layout->addWidget (box, 5, 1);

   box { accepted = search; }
   box { rejected { reject (); } }
   show ();
}

/*
void GrepDialog::openDialog (QTextEdit * editor)
{
   if (editor != null)
   {
      QString text = editor->textCursor ().selectedText ();
      if (text != "")
          pattern->setText (text);
   }
   pattern->selectAll ();
   show ();
}
*/

void GrepDialog::search ()
{
   QString params = pattern->text ();
   params = params.trimmed ();
   if (win != null && params != "")
   {
      if (! caseSensitive->isChecked ())
         params = "-i " + params;

      if (wholeWords->isChecked ())
         params = "-w " + params;

      if (! regularExpression->isChecked ())
         params = "-F " + params;

      QString path = directory->currentText ();
      path = path.trimmed ();
      if (path == "")
          path = ".";
      params = params + " -R " + path;

      accept ();

      win->grep (params);
   }
}

/* ---------------------------------------------------------------------- */

GrepView::GrepView (QWidget * p_win) :
    QWidget (p_win),
    win (p_win)
{
    initVariables ();

    QVBoxLayout * layout = new QVBoxLayout (this);
    setLayout (layout);

    tree = new QTreeWidget (this);
    tree->header ()->hide ();
    tree { itemActivated = onItemActivated; }
    layout->addWidget (tree);

    stopButton = new QPushButton (this);
    stopButton->setText ("stop");
    stopButton->setEnabled (false);
    stopButton->clicked = stopProcess;
    layout->addWidget (stopButton);

    /*
    QVBoxLayout * layout
    {
       tree { }
       tree->header ()->hide ();
       tree { itemActivated = onItemActivated; }

       stopButton
       {
          setText ("stop");
          setEnabled (false);
          clicked { process->terminate (); }
       }
    }
    */
}

void GrepView::initVariables ()
{
    lastFileName = "";
    branch = null;
    initLineVariables ();
}

void GrepView::initLineVariables ()
{
    startLine = true;
    middle = false;
    fileName = "";
    lineNum = 0;
    text = "";
}

void GrepView::showItem ()
{
    if (text != "")
    {
        if (fileName != lastFileName)
            branch = null;

        if (branch == null)
        {
            branch = new TreeItem (tree);
            branch->setText (0, fileName);
            branch->setForeground (0, QColor (Qt::darkGreen));
            // branch->fileName = os.path.abspath (fileName);
            branch->fileName = fileName;
            lastFileName = fileName;
        }

        TreeItem * node = new TreeItem (branch);
        node->setText (0, "Line " + QString::number (lineNum) + ": " + text);
        node->src_line = lineNum;
    }
}

void GrepView::dataReady ()
{
   QByteArray data = process->readAll ();
   for (char c : data)
   {
       if (c == '\n')
       {
          showItem ();
          initLineVariables ();
       }
       else if (startLine)
       {
          if (c != ':')
          {
             fileName += c;
          }
          else
          {
             startLine = false;
             middle = true;
          }
       }
       else if (middle)
       {
          if (c != ':')
          {
             if (c >= '0' && c <= '9')
                lineNum = 10 * lineNum + (c - '0');
          }
          else
          {
              middle = false;
          }
       }
       else
       {
           text += c;
       }
   }
}

void GrepView::stopProcess ()
{
   process.terminate ();
}

void GrepView::onItemActivated (QTreeWidgetItem * node, int column)
{
   fileName = "";
   lineNum = 0;
   TreeItem * item = dynamic_cast < TreeItem * > (node);

   if (item != null)
   {
       lineNum = item->src_line;
       while (fileName == "" && item != null)
       {
           fileName = item->fileName;
           item = dynamic_cast < TreeItem * > (item->parent ());
       }
   }

   if (fileName != null)
      if (win != null)
          ; // win->loadFile (fileName, lineNum);
}

void GrepView::prevItem ()
{
   QTreeWidgetItem * item = tree.currentItem ();
   if (item == null)
   {
      item = tree->topLevelItem (0);
      if (item != null)
      {
         tree->setCurrentItem (item);
         onItemActivated (item, 0);
      }
   }
   else
   {
      QTreeWidgetItem * temp = tree->itemAbove (item);

      if (temp != null)
         tree->expandItem (temp);

      item = tree->itemAbove (item);
      if  (item != null)
      {
         tree->setCurrentItem (item);
         onItemActivated (item, 0);
      }
   }
}

void GrepView::nextItem ()
{
   QTreeWidgetItem * item = tree->currentItem ();
   if (item == null)
   {
      item = tree->topLevelItem (0);
      if (item != null)
      {
         tree->setCurrentItem (item);
         onItemActivated (item, 0);
      }
   }
   else
   {
      QTreeWidgetItem * temp = tree->itemBelow (item);

      if (temp != null)
          tree->expandItem (temp);

      item = tree->itemBelow (item);

      if (item != null)
      {
         tree->setCurrentItem (item);
         onItemActivated (item, 0);
      }
   }
}

void GrepView::grep (QString params)
{
   tree->clear ();
   initVariables ();

   process = new QProcess (this);
   process.setProcessChannelMode (QProcess::MergedChannels);
   process.readyRead = dataReady;
   process.started { stopButton.setEnabled (true); }
   process { finished { stopButton.setEnabled (false); } }
   params = "-n " + params;

   QStringList args;
   args << "-c" << "grep " + params + " -r .";
   process.start ("/bin/sh", args);
}

/* ---------------------------------------------------------------------- */
