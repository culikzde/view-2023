/* files.t */

/* ---------------------------------------------------------------------- */

class IconProvider : public QFileIconProvider
{
   public:
       IconProvider ();
       QIcon icon (QFileInfo fileInfo);
   private:
       QMimeDatabase database;
       QList <QByteArray> formats;
};

IconProvider::IconProvider ()
{
   formats = QImageReader::supportedImageFormats();
}

QIcon IconProvider::icon (QFileInfo fileInfo)
{
   QIcon result;
   QString ext = fileInfo.suffix (); // temporary variable, suffix () type is not known to c2py
   if (formats.contains (ext.toLatin1()))
   {
      result = QIcon (fileInfo.filePath ());
   }
   if (result.isNull ())
   {
      QMimeType info = database.mimeTypeForFile (fileInfo);
      result = QIcon::fromTheme (info.iconName ());
   }
   if (result.isNull ())
   {
      result =  QFileIconProvider::icon (fileInfo);
   }
   return result;
}

/* ---------------------------------------------------------------------- */

class FileView : public QTreeView
{
   public:
      FileView (QWidget * win);
   private:
      void showPath (QString path);
      void onActivated (const QModelIndex & index);
      void onContextMenu (const QPoint & pos);
};

FileView::FileView (QWidget * win) :
   QTreeView (win)
{
   QString path = QDir::currentPath ();

   //  model = QFileSystemModel ();
   //  model.setRootPath (path);
   QDirModel * model = new QDirModel ();

   model->setIconProvider (new IconProvider);

   setModel (model);
   setCurrentIndex (model->index (path));

   QHeaderView * hdr = header ();
   hdr->setSectionResizeMode (0, QHeaderView::ResizeToContents);
   hdr->setSectionResizeMode (1, QHeaderView::Fixed);
   hdr->hideSection (2);
   hdr->hideSection (3);

   activated = onActivated;

   setContextMenuPolicy (Qt::CustomContextMenu);
   customContextMenuRequested = onContextMenu;
}

void FileView::onActivated (const QModelIndex & index)
{
   // if self.win != None :
   QDirModel * m = dynamic_cast < QDirModel *> (model ());
   QString fileName = m->filePath (index);
   // self.win.showStatus (fileName)
   // self.win.loadFile (fileName)
}

void FileView::onContextMenu (const QPoint & pos)
{
   QMenu * menu = new QMenu (this);

   QStringList dir_list;
   // dir_list << sys.path [0];
   dir_list += QIcon::themeSearchPaths();
   dir_list += QIcon::fallbackSearchPaths();

   for (QString path : dir_list)
   {
      QAction * act = menu->addAction (path);
      act.triggered { showPath (path); }
   }

   menu->exec (mapToGlobal (QPoint (pos)));
}

void FileView::showPath (QString path)
{
   QDirModel * m = dynamic_cast < QDirModel *> (model ());
   QModelIndex inx = m->index (path);
   setCurrentIndex (inx);
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
