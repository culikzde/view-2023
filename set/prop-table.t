
/* prop-table.t */

/* ---------------------------------------------------------------------- */

namespace plastic
{
   class Record { };
}

using namespace plastic;


namespace building
{

/* ---------------------------------------------------------------------- */

class BoolIO
{
    void display (QTableWidgetItem * node, bool v)
    {
        node->setData (Qt::EditRole, v);
        node->setData (Qt::UserRole, "bool");
    }

    void store (QTableWidgetItem * node, bool & v)
    {
        v = node->data (Qt::EditRole).var.toBool ();
    }
};

class IntIO
{
    void display (QTableWidgetItem * node, int v)
    {
        node->setData (Qt::EditRole, v);
        node->setData (Qt::UserRole, "int");
    }

    void store (QTableWidgetItem * node, int & v)
    {
        v = node->data (Qt::EditRole).var.toInt ();
    }
};

class DoubleIO
{
    void display (QTableWidgetItem * node, double v)
    {
        node->setData (Qt::EditRole, v);
        node->setData (Qt::UserRole, "double");
    }

    void store (QTableWidgetItem * node, double & v)
    {
        v = node->data (Qt::EditRole).var.toDouble ();
    }
};

class StringIO
{
    void display (QTableWidgetItem * node, QString v)
    {
        node->setData (Qt::EditRole, v);
        node->setData (Qt::UserRole, "string");
    }

    void store (QTableWidgetItem * node, QString & v)
    {
        v = node->data (Qt::EditRole).var.toString ();
    }
};

class StringListIO
{
    void display (QTableWidgetItem * node, QStringList v)
    {
        node->setData (Qt::EditRole, v);
        node->setData (Qt::UserRole, "list");
    }

    void store (QTableWidgetItem * node, QString & v)
    {
        v = node->data (Qt::EditRole).var.toStringList ();
    }
};

class ColorIO
{
    void display (QTableWidgetItem * node, QColor v)
    {
        node->setData (Qt::EditRole, v);
        node->setData (Qt::UserRole, "color");
    }

    void store (QTableWidgetItem * node, QColor & v)
    {
        v = node->data (Qt::EditRole).var.value <QColor> ();
    }
};

class FontIO
{
    void display (QTableWidgetItem * node, QFont v)
    {
        node->setData (Qt::EditRole, v);
        node->setData (Qt::UserRole, "font");
    }

    void store (QTableWidgetItem * node, QFont & v)
    {
        v = node->data (Qt::EditRole).var.value <QFont> ();
    }
};

/* ---------------------------------------------------------------------- */

} // end of namespace building

using namespace building;

namespace set
{

/* ---------------------------------------------------------------------- */

class Properties : public PropertyTable
{
public:
    Record * data;

    void init ()
    {
    }

    void display ()
    {
    }

    void store ()
    {
    }

    Properties (QWidget * parent) :
       PropertyTable (parent),
       data (null)
    {
       init ();
       // display ();
       show ();
    }

    void storeData ()
    {
       if (data != null) store ();
    }

    void setData (Record * p_data)
    {
       storeData ();
       data = p_data;
       if (data != null) display ();
    }
};

/* ---------------------------------------------------------------------- */

} // end of namespace set

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
