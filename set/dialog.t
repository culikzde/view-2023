
/* dialog.t */

/* ---------------------------------------------------------------------- */

namespace plastic
{
   class Record { };
}

using namespace plastic;

namespace building
{

/* ---------------------------------------------------------------------- */

void addItem (QFormLayout * layout, QString title, QWidget * widget)
{
    layout->addRow (title, widget);
}

/* ---------------------------------------------------------------------- */

class CheckBoxIO
{
    QCheckBox * checkbox;

    void init ()
    {
        checkbox = new QCheckBox (this);
    }

    void display (bool b)
    {
        checkbox->setChecked (b);
    }

    void store (bool & b)
    {
        b = checkbox->isChecked ();
    }
};

/* ---------------------------------------------------------------------- */

class SpinBoxIO
{
    QSpinBox * spin;

    void init ()
    {
        spin = new QSpinBox (this);
    }

    void display (int n)
    {
        spin->setValue (n);
    }

    void store (int & n)
    {
        n = spin->value ();
    }

};

class DoubleSpinBoxIO
{
    QDoubleSpinBox * dbl_spin;

    void init ()
    {
        dbl_spin = new QDoubleSpinBox (this);
    }

    void display (double x)
    {
        dbl_spin->setValue (x);
    }

    void store (double & x)
    {
        x = dbl_spin->value ();
    }

};

/* ---------------------------------------------------------------------- */

class LineEditIO
{
    QLineEdit * edit;

    void init ()
    {
        edit = new QLineEdit (this);
    }

    void display (QString s)
    {
        edit->setText (s);
    }

    void store (QString & s)
    {
        s = edit->text ();
    }
};

/* ---------------------------------------------------------------------- */

} // end of namespace building

using namespace building;

namespace set
{

/* ---------------------------------------------------------------------- */

class Dialog : public QDialog
{
public:
    Record * data;

    void init (QFormLayout * layout)
    {
    }

    void display ()
    {
    }

    void store ()
    {
    }

    // accepted { store (); }
    // finished { store (); }

    Dialog (Record * p_data) :
       data (p_data)
    {
       // QVBoxLayout * layout = new QVBoxLayout;
       QFormLayout * layout = new QFormLayout;
       setLayout (layout);
       init (layout);

       QDialogButtonBox * box = new QDialogButtonBox (QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
       layout->addRow ("", box);
       QObject::connect (box, &QDialogButtonBox::accepted, this, accept);
       QObject::connect (box, &QDialogButtonBox::rejected, this, reject);

       connect (this, SIGNAL (accepted ()), this, SLOT (store));

       display ();
       show ();
    }
};

/* ---------------------------------------------------------------------- */

} // end of namespace set

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
