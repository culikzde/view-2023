
class QJsonObject;
class QJsonDocument;
class QJsonValue;

class QApplication;
class QClipboard;

class QMimeData;
class QByteArray;

#include <QJsonObject>
#include <QJsonDocument>

#include <QApplication>
#include <QClipboard>
#include <QMimeData>

class Record;
Record * readRecord (const QJsonObject obj);
QJsonObject writeRecord (Record * r);

/* ---------------------------------------------------------------------- */

const QString jsonFormat = "application/json";

void copyToClipboard (Record * input)
{
    QJsonObject obj = writeRecord (input);
    QJsonDocument doc (obj);
    QByteArray code = doc.toJson ();

    QMimeData * data = new QMimeData;
    data->setData (jsonFormat, code);

    QClipboard * clip = QApplication::clipboard ();
    clip->setMimeData (data);
}

Record * recallFormMimeData (const QMimeData * data)
{
    Record * result = NULL;
    if (data->hasFormat (jsonFormat))
    {
       QByteArray code = data->data (jsonFormat);
       QJsonDocument doc = QJsonDocument::fromJson (code);
       QJsonObject obj = doc.object ();
       result = readRecord (obj);
    }
    return result;
}

Record * pasteFromClipboard ()
{
    Record * result = NULL;
    QClipboard * clip = QApplication::clipboard ();
    const QMimeData * data = clip->mimeData ();
    return recallFormMimeData (data);
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
