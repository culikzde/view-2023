
# allbatross.py
# Python Language Server
# https://github.com/palantir/python-language-server

import os, sys

if __name__ == '__main__':
   work_dir = os.path.abspath (sys.path [0])
   sys.path.insert (1, work_dir)

from palantir import * # local module

# --------------------------------------------------------------------------

class AlbatrossLanguageServer (MethodDispatcher):

    def __init__(self, rx, tx):
        self._jsonrpc_stream_reader = JsonRpcStreamReader(rx)
        self._jsonrpc_stream_writer = JsonRpcStreamWriter(tx)
        self._endpoint = Endpoint(self, self._jsonrpc_stream_writer.write, max_workers=MAX_WORKERS)

    def start(self):
        """Entry point for the server."""
        self._jsonrpc_stream_reader.listen(self._endpoint.consume)

    def m_exit(self, **_kwargs):
        self._endpoint.shutdown()
        self._jsonrpc_stream_reader.close()
        self._jsonrpc_stream_writer.close()

    def m_initialize(self, processId=None, rootUri=None, rootPath=None, initializationOptions=None, **_kwargs):
        log.debug('Language server initialized with %s %s %s %s', processId, rootUri, rootPath, initializationOptions)
        if rootUri is None:
            rootUri = uris.from_fs_path(rootPath) if rootPath is not None else ''

        # Get our capabilities
        return {'capabilities': self.capabilities()}

    def capabilities(self):
        server_capabilities = {
            # 'codeActionProvider': True,
            # 'codeLensProvider': {
            #     'resolveProvider': False,  # We may need to make this configurable
            # },
            'completionProvider': {
                'resolveProvider': False,  # We know everything ahead of time
                'triggerCharacters': ['.']
            },
            'documentFormattingProvider': True,
            'documentHighlightProvider': True,
            'documentRangeFormattingProvider': True,
            'documentSymbolProvider': True,
            'definitionProvider': True,
            'executeCommandProvider': {
                 'commands': [ 'cmd' ]
            },
            'hoverProvider': True,
            'referencesProvider': True,
            # 'renameProvider': True,
            'foldingRangeProvider': True,
            'signatureHelpProvider': {
                 'triggerCharacters': ['(', ',']
            },
            # 'textDocumentSync': {
            #     'change': lsp.TextDocumentSyncKind.INCREMENTAL,
            #     'save': {
            #         'includeText': True,
            #     },
            #     'openClose': True,
            # },
            # 'workspace': {
            #     'workspaceFolders': {
            #         'supported': True,
            #         'changeNotifications': True
            #     }
            # },
            # 'experimental': { }
        }
        log.info('Server capabilities: %s', server_capabilities)
        return server_capabilities

    def m_text_document__document_symbol(self, textDocument=None, **_kwargs):
        doc_uri = textDocument['uri']
        return [ { "name": "alpha"}, {"name": "beta"}, {"name": "gamma"} ]

    def m_text_document__completion(self, textDocument=None, position=None, **_kwargs):
        doc_uri = textDocument['uri']
        return {
            'isIncomplete': False,
            'items': [
                { "label": "abc" },
                { "label": "def" },
             ]
        }

    def m_text_document__hover(self, textDocument=None, position=None, **_kwargs):
        contents = [
            {
                'language': 'python',
                'value': 'something',
            }
        ]
        return {'contents': contents}

    def m_text_document__formatting(self, textDocument=None, _options=None, **_kwargs):
        return [{
           'range': {
               'start': {'line': 1, 'character': 0},
               'end': {'line': 1, 'character': 3}
           },
           'newText': 'new text'
        }]

    def m_text_document__range_formatting(self, textDocument=None, range=None, _options=None, **_kwargs):
        return [{
           'range': {
               'start': {'line': 1, 'character': 0},
               'end': {'line': 1, 'character': 3}
           },
           'newText': 'new text'
        }]

    def m_text_document__document_highlight(self, textDocument=None, position=None, **_kwargs):
        return [
                  {
                     'range': { 'start': {'line': 0, 'character': 0}, 'end': {'line': 0, 'character': 3}},
                     'kind': lsp.DocumentHighlightKind.Read
                  },
                  {
                     'range': { 'start': {'line': 2, 'character': 0}, 'end': {'line': 2, 'character': 3}},
                     'kind': lsp.DocumentHighlightKind.Write
                  },
        ]

    def m_text_document__signature_help(self, textDocument=None, position=None, **_kwargs):
        return {
           'signatures': [
               {
                  'label': 'some function',
                  'documentation': 'documentation',
                  'parameters': [{ 'label': 'parameter', 'documentation': 'parameter documentation' }]
               }
            ],
            'activeSignature': 0,
            'activeParameter': 0,
        }

    def m_text_document__definition(self, textDocument=None, position=None, **_kwargs):
        doc_uri = textDocument['uri']
        return [
           {
              'uri': doc_uri,
              'range': {
                  'start': {'line': 3, 'character': 1},
                  'end': {'line': 3, 'character': 3},
               }
           }
        ]

    def m_text_document__references(self, textDocument=None, position=None, context=None, **_kwargs):
        exclude_declaration = not context['includeDeclaration']
        doc_uri = textDocument['uri']
        return [{
           'uri': doc_uri,
           'range': {
               'start': {'line': 3, 'character': 1},
               'end': {'line': 3, 'character': 3}
           }
       }]

    def m_text_document__folding_range(self, textDocument=None, **_kwargs):
        results = []
        results.append ({'startLine': 3, 'endLine': 5, })
        return results

    def m_workspace__execute_command(self, command=None, arguments=None):
        pass

# --------------------------------------------------------------------------

if __name__ == '__main__':
   language_server (AlbatrossLanguageServer)

# Qt Creator :
# menu Help / About Plugins : Other Languages / Language Client ... enable
# menu Tools / Options : Language Client
# /usr/bin/python3
# /abc/gibbon/view/code/albatross.py -v -v --log-file /abc/albatross.log
# file pattern: *.mo

# see Qt Creator Outline

# qt-creator/src/libs/languageserverprotocol/jsonkeys.h
# qt-creator/src/plugins/languageclient
