
# creator.py

from __future__ import print_function

import os

from util import import_qt_modules
import_qt_modules (globals ()) # QProcess

from util import findIcon
from tools import CommandData
from lexer import Lexer

from llvm_code import use_clang, clang_compile, clang_import_tree

if use_clang :
   from clang.cindex import Index, Config, CursorKind, TypeKind, TranslationUnit

# --------------------------------------------------------------------------

def read_pro_file (self, projFileName) :

    config = []
    headers = []
    sources = []
    forms = []
    cond = True

    input = Lexer ()
    input.configuration = True
    input.openFile (projFileName)

    while not input.isEndOfSource () :
       name = ""
       if input.isKeyword ("if") :
          input.nextToken ()
          if input.isSeparator ('(') :
             input.nextToken ()
             if input.isKeyword ("true") or input.tokenText == "1" :
                input.nextToken ()
                cond = True
             elif input.isKeyword ("false") or input.tokenText == "0" :
                input.nextToken ()
                cond = False
             if input.isSeparator ('(') :
                input.nextToken ()
             if input.isSeparator ('{') :
                input.nextToken ()
       elif input.isSeparator ('}') :
          cond = True
          input.nextToken ()
       elif input.isIdentifier () :
          text = input.readIdentifier ()
          if input.isSeparator ('{') :
             input.nextToken ()
             cond = text in config
             # print (text, cond)
          elif input.isSeparator ('+=') :
             input.nextToken ()
             name = text

       while input.token != input.eos and input.token != input.end_of_line :
          if name != ""  and cond:
             if input.token == input.identifier :
                value = input.tokenText
                print (name, "<-", value)
                if name == "CONFIG" :
                   config.append (value)
                if name == "HEADERS" :
                   headers.append (value)
                elif name == "SOURCES" :
                   sources.append (value)
                if name == "FORMS" :
                   forms.append (value)
          input.configurationToken ()

       if input.token == input.end_of_line :
          input.nextToken ()

    input.close ()

    projDir = os.path.dirname (projFileName)
    self.initProject (projFileName)
    fileNames = [ ]

    self.openSubProject ("headers")
    for name in headers :
        fileName = os.path.join (projDir, name)
        fileNames.append (fileName)
        # self.loadFile (fileName)
        node = self.joinProject (fileName)
        node.setIcon (0, findIcon("text-x-c++hdr"))
    self.closeSubProject ()

    self.openSubProject ("sources")
    for name in sources :
        fileName = os.path.join (projDir, name)
        fileNames.append (fileName)
        # self.loadFile (fileName)
        node = self.joinProject (fileName)
        node.setIcon (0, findIcon("text-x-c++src"))
    self.closeSubProject ()

    self.openSubProject ("forms")
    for name in forms :
        fileName = os.path.join (projDir, name)
        # self.loadFile (fileName)
        node = self.joinProject (fileName)
        node.setIcon (0, findIcon("application-x-designer"))
    self.closeSubProject ()

    # read_code (self, fileNames)
    clang_import_tree (self, fileNames)

def read_code (self, fileNames) :
    desc = CommandData ()
    desc.plugin = "cmm"
    desc.sourceFileNames = fileNames
    desc.ignore_all_includes = True
    desc.compilerOptions = "-D signals -D slots -D QT_BEGIN_NAMESPACE -D QT_END_NAMESPACE -D Q_OBJECT -D Q_INVOKABLE -D Q_PROPERTY"
    desc.pythonClassName = "-"
    desc.cppClassName = "-"
    self.info.runCommandObject (desc)

# --------------------------------------------------------------------------

# Qt Creator

def check_error (exit_code, cmd) :
    if exit_code != 0 :
       print ("Error executing " + cmd)

def open_qt_creator (win) :
    fileName = "examples/design/example.pro"
    # fileName = QFileDialog.getOpenFileName (win, "Open File")
    # fileName = dialog_to_str (fileName)
    dirName, fileName = os.path.split (fileName)
    cmd = "cd " + dirName + " ; qtcreator " + fileName + " example.ui"
    process = QProcess (win)
    process.finished.connect (lambda exit_code : check_error (exit_code, cmd))
    process.start ("/bin/sh", [ "-c", cmd ] )

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
