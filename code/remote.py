
# remote.py

from __future__ import print_function


import os

use_paramiko = True

if use_paramiko :
   try :
       # dnf install python3-paramiko python3-scp
       # python -m pip install scp
       import paramiko
       from paramiko import SSHClient
       from paramiko.config import SSHConfig
       from scp import SCPClient
       from glob import glob
       # import os
   except :
      use_paramiko = False
      print ("missing paramiko")

# --------------------------------------------------------------------------

def run (ssh_client, cmd) :
    print ("RUN:", cmd)
    stdin, stdout, stderr = ssh_client.exec_command (cmd)
    for line in stderr.readlines () :
       print ("error:", line, end="")
    for line in stdout.readlines () :
       print (line, end="")

def notebook_example () :

    ssh_client = SSHClient ()
    ssh_client.load_system_host_keys ()
    ssh_client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())

    print ("connecting ...")
    ssh_client.connect ("10.0.0.2")

    run (ssh_client, "test -d temp/test || mkdir -p temp/test")

    scp_client = SCPClient (ssh_client.get_transport())

    scp_client.put ("examples/cuda-mini.cu", "temp/test/")

    run (ssh_client, "cd temp/test && " +
                     "PATH=/usr/local/cuda/bin:$PATH " +
                     "nvcc -gencode arch=compute_50,code=sm_50 cuda-mini.cu -o cuda-mini.bin")

    # run (ssh_client, "ls -l temp/test")

    run (ssh_client, "cd temp/test && ./cuda-mini.bin")

    scp_client.close ()
    ssh_client.close ()

def example () :

    host = "gp4"

    config = SSHConfig.from_path (os.path.expanduser ('/abc/key/config'))
    conf = config.lookup (host)
    # print ("CONF", conf)
    hostname = conf ["hostname"]
    username = conf ["user"]
    key_filename = os.path.expanduser ("/abc/key/gp_id_rsa")

    ssh_client = SSHClient ()
    ssh_client.load_system_host_keys ()
    ssh_client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())

    print ("connecting", hostname, "...")

    ssh_client.connect (hostname, username = username, key_filename = key_filename)
    scp_client = SCPClient (ssh_client.get_transport())

    dest = "temp/test"
    run (ssh_client, "test -d " + dest + " || mkdir -p " + dest)

    src_dir = "../kit/cuda-owl/"
    file_list = glob (src_dir + "/*.cu") + glob (src_dir + "/*.cuh") + glob (src_dir + "/*.h") + glob (src_dir + "/*.cc") + glob (src_dir + "/*.cpp") + glob (src_dir + "/makefile")
    print (file_list)

    for file_name in file_list :
        scp_client.put (file_name, dest)

    run (ssh_client, "cd " + dest + " && make")
    run (ssh_client, "ls -l " + dest)

    run (ssh_client, "cd " + dest + " && make run")

    scp_client.close ()
    ssh_client.close ()

def forward_example () :

    host = "gp4"
    gw_host = "kmlinux"

    config = SSHConfig.from_path (os.path.expanduser ('/abc/key/config'))

    gw_conf = config.lookup (gw_host)
    gw_hostname = gw_conf ["hostname"]
    gw_username = gw_conf ["user"]
    gw_key_filename = os.path.expanduser (gw_conf ["identityfile"] [0])

    conf = config.lookup (host)
    hostname = conf ["hostname"]
    username = conf ["user"]
    key_filename = os.path.expanduser (conf ["identityfile"] [0])

    from forward import forward_tunnel
    # https://github.com/paramiko/paramiko/blob/main/demos/forward.py

    print ("connecting", gw_hostname, "...")

    gw_client = SSHClient ()
    gw_client.load_system_host_keys ()
    gw_client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())
    gw_client.connect (gw_hostname, port = 22, username = gw_username, key_filename = gw_key_filename)

    print ("forwarding to", hostname, "...")

    # forward_tunnel (2222, hostname, 22, ssh_client.get_transport())
    from threading import Thread
    thread = Thread (target = forward_tunnel, args = ( 2222, hostname, 22, gw_client.get_transport() ))
    thread.start()
    # thread.join()

    ssh_client = SSHClient ()
    ssh_client.load_system_host_keys ()
    ssh_client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())

    ssh_client.connect ("localhost", port = 2222, username = username, key_filename = key_filename)
    scp_client = SCPClient (ssh_client.get_transport())

    print ()
    run (ssh_client, "hostnamectl")
    print ()

    dest = "temp/test"
    run (ssh_client, "test -d " + dest + " || mkdir -p " + dest)

    src_dir = "../kit/cuda-owl/"
    file_list = glob (src_dir + "/*.cu") + glob (src_dir + "/*.cuh") + glob (src_dir + "/*.h") + glob (src_dir + "/*.cc") + glob (src_dir + "/*.cpp") + glob (src_dir + "/makefile")
    print (file_list)

    for file_name in file_list :
        scp_client.put (file_name, dest)

    run (ssh_client, "cd " + dest + " && make")
    run (ssh_client, "ls -l " + dest)

    run (ssh_client, "cd " + dest + " && make run")

    run (ssh_client, "cat " + dest + "/new.txt")

    scp_client.close ()
    ssh_client.close ()

    # gw_client.close ()

def tunnel_example () :

    host = "gp4"
    gw_host = "kmlinux"

    config = SSHConfig.from_path (os.path.expanduser ('/abc/key/config'))

    gw_conf = config.lookup (gw_host)
    gw_hostname = gw_conf ["hostname"]
    gw_username = gw_conf ["user"]
    gw_key_filename = os.path.expanduser (gw_conf ["identityfile"] [0])

    conf = config.lookup (host)
    hostname = conf ["hostname"]
    username = conf ["user"]
    key_filename = os.path.expanduser (conf ["identityfile"] [0])

    print ("tunneling through", gw_hostname, "...")

    import sshtunnel

    with sshtunnel.open_tunnel (
            ssh_address_or_host = gw_hostname,
            ssh_username = gw_username,
            ssh_pkey = gw_key_filename,
            remote_bind_address = (hostname, 22),
            local_bind_address = ('0.0.0.0', 10022)
       ) as tunnel:
          ssh_client = paramiko.SSHClient ()
          ssh_client.load_system_host_keys ()
          ssh_client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())

          print ("connecting", hostname, "...")

          ssh_client.connect ('127.0.0.1', port = 10022, username = username, key_filename = key_filename)
          scp_client = SCPClient (ssh_client.get_transport())

          print ()
          run (ssh_client, "hostnamectl")
          print ()

          dest = "temp/test"
          run (ssh_client, "test -d " + dest + " || mkdir -p " + dest)

          src_dir = "../kit/cuda-owl/"
          file_list = glob (src_dir + "/*.cu") + glob (src_dir + "/*.cuh") + glob (src_dir + "/*.h") + glob (src_dir + "/*.cc") + glob (src_dir + "/*.cpp") + glob (src_dir + "/makefile")
          print (file_list)

          for file_name in file_list :
              scp_client.put (file_name, dest)

          run (ssh_client, "cd " + dest + " && make")
          run (ssh_client, "ls -l " + dest)

          run (ssh_client, "cd " + dest + " && make run")

          scp_client.close ()
          ssh_client.close ()

def proxy_example () :

    host = "gp4"
    gw_host = "kmlinux"

    config = SSHConfig.from_path (os.path.expanduser ('/abc/key/config'))

    gw_conf = config.lookup (gw_host)
    gw_hostname = gw_conf ["hostname"]
    gw_username = gw_conf ["user"]
    gw_key_filename = os.path.expanduser (gw_conf ["identityfile"] [0])

    conf = config.lookup (host)
    hostname = conf ["hostname"]
    username = conf ["user"]
    key_filename = os.path.expanduser (conf ["identityfile"] [0])

    print ("proxy ", gw_hostname, "...")

    gw_client = paramiko.SSHClient ()
    gw_client.load_system_host_keys ()
    gw_client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())
    gw_client.connect (gw_hostname, port = 22, username = gw_username, key_filename = gw_key_filename)

    sock = gw_client.get_transport().open_channel ('direct-tcpip', (hostname, 22), ('', 0))
    # http://github.com/paramiko/paramiko/issues/1018
    # http://datatracker.ietf.org/doc/html/rfc4254#section-7.2

    ssh_client = paramiko.SSHClient ()
    ssh_client.load_system_host_keys ()
    ssh_client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())

    print ("connecting", hostname, "...")

    ssh_client.connect (hostname = hostname, username = username, key_filename = key_filename, sock = sock)
    scp_client = SCPClient (ssh_client.get_transport())

    print ()
    run (ssh_client, "hostnamectl")
    print ()

    dest = "temp/test"
    run (ssh_client, "test -d " + dest + " || mkdir -p " + dest)

    src_dir = "../kit/cuda-owl/"
    file_list = glob (src_dir + "/*.cu") + glob (src_dir + "/*.cuh") + glob (src_dir + "/*.h") + glob (src_dir + "/*.cc") + glob (src_dir + "/*.cpp") + glob (src_dir + "/makefile")
    print (file_list)

    for file_name in file_list :
        scp_client.put (file_name, dest)

    run (ssh_client, "cd " + dest + " && make")
    run (ssh_client, "ls -l " + dest)

    run (ssh_client, "cd " + dest + " && make run")

    scp_client.close ()
    ssh_client.close ()

    gw_client.close ()


def helios_example () :

    host = "helios"

    config = SSHConfig.from_path (os.path.expanduser ('/abc/key/config'))
    conf = config.lookup (host)
    # print ("CONF", conf)
    hostname = conf ["hostname"]
    username = conf ["user"]
    key_filename = os.path.expanduser ("/abc/key/gp_id_rsa")

    print ("connecting", hostname, "...")

    ssh_client = SSHClient ()
    ssh_client.load_system_host_keys ()
    ssh_client.set_missing_host_key_policy (paramiko.AutoAddPolicy ())

    ssh_client.connect (hostname, username = username, key_filename = key_filename)
    scp_client = SCPClient (ssh_client.get_transport())

    run (ssh_client, "test -d temp/test || mkdir -p temp/test")

    src_dir = "../misc/orange-owl/"
    file_list = glob (src_dir + "/*.cu") + glob (src_dir + "/*.cuh") + glob (src_dir + "/*.h") + glob (src_dir + "/*.cpp")
    print (file_list)

    for file_name in file_list :
        scp_client.put (file_name, "temp/test/")

        scp_client.put (src_dir + "/makefile-helios", "temp/test/makefile")
        scp_client.put (src_dir + "/run.cfg", "temp/test/")

    run (ssh_client, "module load cuda/10.2")
    run (ssh_client, "cd temp/test && make")
    # run (ssh_client, "ls -l temp/test")

    # run (ssh_client, "cd test && qsub -q gpu -j oe -N Pokus_CUDA -l walltime=01:00:00 -l select=1:mem=4G:ncpus=4:ngpus=1 -- ./heat")
    run (ssh_client, "cd temp/test && qsub run.cfg")

    run (ssh_client, "qstat")
    # run (ssh_client, "qstat -q gpu")
    # run (ssh_client, "cat temp/test/new.txt")

    # qdel job_number

    scp_client.close ()
    ssh_client.close ()

# --------------------------------------------------------------------------

# .ssh/config

# Host ...
#    HostName ...
#    User ...
#    ServerAliveInterval 60
#    ProxyJump ...
#    IdentityFile ...
#    ForwardX11 yes

ssh_host = "gp4"
ssh_opt = "-o ServerAliveInterval=60"

def ssh (cmd) :
    cmd = "ssh " + ssh_host + " " + ssh_opt + " '( " + cmd + " )'"
    print (cmd)
    os.system (cmd)

def scp (source, destination) :
    os.system ("scp " + ssh_opt + " " + source + " " + ssh_host + ":" + destination)

def cmd_example () :

    dest = "temp/test"
    ssh ("test -d " + dest + " || mkdir -p " + dest)

    src_dir = "../kit/cuda-owl/"
    from glob import glob
    file_list = glob (src_dir + "/*.cu") + glob (src_dir + "/*.cuh") + glob (src_dir + "/*.h") + glob (src_dir + "/*.cc") + glob (src_dir + "/*.cpp") + glob (src_dir + "/makefile")

    for file_name in file_list :
        scp (file_name, dest)

    ssh ("cd " + dest + " && make")
    ssh ("ls -l " + dest)

    ssh ("cd " + dest + " && make run")

# --------------------------------------------------------------------------


def remote_cuda_file (win) :

    if not use_paramiko :
       print ("Missing paramiko")
       return

    win.info.clearOutput ()

    # notebook_example ()

    # example ()
    # forward_example ()
    # tunnel_example ()
    # proxy_example ()

    # helios_example ()

    cmd_example ()

# --------------------------------------------------------------------------



# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
