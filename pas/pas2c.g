
/* pas2c.g */

/* ---------------------------------------------------------------------- */

index_expr_item <new TInxItem> :
   '[' ref:expr ']';

index_expr_list <new TInxList> :
   <add> index_expr_item ( ',' <add> index_expr_item )* ;

/* arguments */

arg_item <TArgItem> :
   value:expr
   ;

arg_list <TArgSect> :
   <add> arg_item ( ',' <add> arg_item )* ;

/* factor */

nil_expr <TNilExpr:TExpr> :
   "null_ptr" ;

not_expr <TNotExpr:TExpr> :
   "!" param:factor ;

adr_expr <TAdrExpr:TExpr> :
   '&' param:factor ;

index_expr <TIndexExpr:TExpr> :
   <store left:TExpr>
   list:index_expr_list
   ;

call_expr <TCallExpr:TExpr> :
   <store func:TExpr>
   '('
   list:arg_list
   ')'
   ;

deref_expr <TDerefExpr:TExpr> :
   '*'
   <store param:TExpr>
   ;

/* term */

term <choose TExpr> :
  factor
  (
     <new TTermExpr:TExpr>
     <store left:TExpr>
     (  '*'   <set fce=MulExp>  |
        '/'   <set fce=RDivExp> |
        '/'   <set fce=DivExp>  |
        '%'   <set fce=ModExp>  |
        "<<"  <set fce=ShlExp>  |
        ">>"  <set fce=ShrExp>  |
        "&"   <set fce=AndExp>  |
        "as"  <set fce=AsExp >  )
     right:factor
  )* ;

/* simple expression */

simple_expr <choose TExpr> :
  term
  (
     <new TSimpleExpr:TExpr>
     <store left:TExpr>
     (  '+'   <set fce=AddExp> |
        '-'   <set fce=SubExp> |
        "|"   <set fce=OrExp>  |
        "^"  <set fce=XorExp> )
     right:term
  )* ;

/* expression */

expr <choose TExpr> :
  simple_expr
  (
     <new TCompleteExpr:TExpr>
     <store left:TExpr>
     (  "=="  <set fce=EqExp> |
        "!=" <set fce=NeExp> |
        '<'  <set fce=LtExp> |
        '>'  <set fce=GtExp> |
        "<=" <set fce=LeExp> |
        ">=" <set fce=GeExp> |
        "in" <set fce=InExp> |
        "is" <set fce=IsExp> )
     right:simple_expr
  )? ;

/* ---------------------------------------------------------------------- */

/* goto */

goto_stat <TGotoStat:TStat> :
  "goto" goto_lab:label_ident ;

/* begin end */

begin_stat <TBlockStat:TStat> :
  '{'
  body_seq:stat_list
  '}' ;

stat_list <TStatSeq> :
   <new_line>
   <indent>
   ( <new_line> <add> stat )*
   <unindent>
   <new_line> ;

/* if */

if_stat <TIfStat:TStat> :
  "if"
  '(' cond_expr:expr ')'
  then_stat:inner_stat
  (
     <silent>
     "else"
     else_stat:inner_stat
  )? ;

/* case */

case_stat <TCaseStat:TStat> :
  "switch"
  '(' case_expr:expr ')'
  <new_line>
  '{'
  <indent>
  case_list:case_sect
  (
    "default" ':'
    else_seq:stat_list
  )?
  <unindent>
  '}' ;

case_sect <TCaseSect> :
  ( <add> case_item )*;

case_item <TCaseItem> :
   <newline>
   "case "
   sel_list:elem_sect
   ':'
   <newline>
   <indent>
   sel_stat:inner_stat
   <unindent>
   <newline>
   "break" ';'
   ;

/* while */

while_stat <TWhileStat:TStat> :
  "while" cond_expr:expr "do" body_stat:inner_stat;

/* repeat */

repeat_stat <TRepeatStat:TStat> :
  "repeat"
  body_seq:stat_list
  "until" until_expr:expr ;

/* for */

for_stat <TForStat:TStat> :
  "for"
  '('
  var_expr:variable
  '='
  from_expr:expr
  ';'

  to_expr:expr
  ( <set incr=true> "<=" | <set incr=false> ">=" )
  var_expr:variable
  ';'

  var_expr:variable
  ( <set incr=true> "++" | <set incr=false> "--" )
  ')'
  body_stat:inner_stat ;

/* with */

with_stat <TWithStat:TStat> :
  "with" with_list:with_sect "do" body_stat:inner_stat ;

with_sect <TWithSect> :
  <add> with_item ( ',' <add> with_item )* ;

with_item <TWithItem> :
  expr:variable ;

/* raise */

raise_stat <TRaiseStat:TStat> :
  "raise" ( raise_expr:expr )? ;

/* try */

try_stat <TTryStat:TStat> :
  "try"
  body_seq:stat_list
  (
    finally_branch:finally_part
  |
    except_branch:except_part
  )
  "end" ;

finally_part <TFinallyPart> :
  "finally"
  finally_seq:stat_list ;

except_part <TExceptPart> :
  "except"
  (
     on_list:on_sect
     ( "else" else_seq:stat_list )?
  |
     else_seq:stat_list
  );

on_sect <TOnSect> :
  <add>  on_item
  ( ';' <add> on_item )*
  ( ';' <set semicolon=true> )? ;

on_item <TOnItem> :
  "on"
  ( on_ident:identifier ':' )?
  on_type:type
  "do"
  body_stat:inner_stat ;

/* empty statement */

empty_stat <TEmptyStat:TStat> : ;

/* simple statement */

simple_stat <choose TStat> :
  expr
  (
     assign_stat
  |
     call_stat
  ) ;

assign_stat <TAssignStat:TStat> :
  <store left_expr:TExpr>
  ":="
  right_expr:expr
  ;

call_stat <TCallStat:TStat> :
  <store call_expr:TExpr>
  ;

/* ---------------------------------------------------------------------- */

param_ident <TParamIdent> :
  name:identifier
  <execute on_param>
  ;

param_item <TParamItem> :
  ( "const" <set mode=ConstParam> )?

  ( typ:param_type )?

  ( '&' <set mode=VarParam> |
    '&'  <set mode=OutParam> )

  <add> param_ident ( ',' <add> param_ident )*
  ( '=' ini:expr )?
  ;

formal_param_list <TParamSect> :
   (
      '('
          <add> param_item
          ( ';' <add> param_item )*
      ')'
   )? ;

/* ---------------------------------------------------------------------- */

class_type <TClassType:TType> :
   "class"
   (
     ':'
     "public" parent:ifc_type
     ( ',' ifc_list:interface_sect )?
   )?
   <indent>
   "public" ':' <newline>
   components:components_sect
   <unindent>
   "end" ;

components_sect <TComponetsSect> :
   ( <add> components_item )* ;

components_item <TComponentsItem> :
   ( "private" ':'  <set acs=PrivateAccess> |
     "protected" ':'<set acs=ProtectedAccess> |
     "public"   ':' <set acs=PublicAccess> |
     unknown_access )
   members:member_sect ;

unknown_access : ;

/* field */

field_ident <TFieldItem> :
   name:identifier
   <execute on_field> ;

field_decl <TFieldDecl:TDeclSect> :
   typ:type
   <add> field_ident
   ( ',' <add> field_ident )*
   ';'
   <execute add_field>
   ;

/* method */

method_decl <TProcDecl> :
   ( "virtual" <set a_virtual=true> )?
   <modify> proc_head
   (  "override" <set a_override=true> )?
   <execute close_proc> ;


/* enum */

enum_type <TEnumType:TType> :
   "enum"
   '{' elements:enum_sect '}' ;

/* record */

record_type <TRecordType:TType> :
  "struct"
  '{'
  <indent>
  fields:field_sect
  <unindent>
  '}'
  "end"
  ;

/* pointer */

pointer_type <TPointerType:TType> :
   elem:type '*';

/* ---------------------------------------------------------------------- */

/* labels */

/* constants */

const_decl <TConstDecl> :
   "const"
   ( typ:type )?
   name:identifier
   '=' val:expr ';'
   ;

const_sect <TConstSect:TDeclSect> :
   ( <add> const_decl )* ;

/* types */

type_decl <TTypeDecl> :
   "typedef"
   typ:type
   name:identifier
   ';' ;

type_sect <TTypeSect:TDeclSect> :
   ( <add> type_decl )* ;

/* variables */

var_item <TVarItem> :
   name:identifier
   <execute on_var>
   ;

var_decl <TVarDecl> :
   typ:type
   <add> var_item ( ',' <add> var_item )*
   ( '=' ini:expr )?
   ';' ;

var_sect <TVarSect:TDeclSect> :
   ( <new_line> <add> var_decl )*

/* subroutine */

proc_head <modify TProcDecl:TDeclSect> :
   ( "static" <set a_static=true> )?

   (
      "void" <set style=ProcedureStyle>
   |
      answer:result_type
   ) ?

   proc_name:qualified_name
   param_list:formal_param_list

   (
     "overload" <set a_overload=true>
   )?

   /*
   (
     ( ';' <set semicolon2=true> )?
     ( "register" <set call_conv=RegisterCall> |
       "pascal"   <set call_conv=PascalCall>   |
       "cdecl"    <set call_conv=CdeclCall>    |
       "stdcall"  <set call_conv=StdcallCall>  |
       "safecall" <set call_conv=SafecallCall> )
     <set call_spec=true>
   )?
   */

   ';' ;

proc_decl <TProcDecl> :
   ( "inline" <set a_inline=true> )?
   ( "external" <set a_external=true> )?
   <modify> proc_head
   (
      ';' <set a_forward=true>
   |
      <modify> proc_external <set external_spec=true>
   |
     <new_line>
     '{'
     <indent>
     local:decl_part
     body:stat_list
     <unindent>
     '}'
     <empty_line> ;
   )

/* declarations */

decl <select TDeclSect> :
   const_sect |
   type_sect |
   var_sect |
   proc_decl ;

decl_part <TDeclGroup> :
   ( <new_line> <add> decl )* ;

/* import */

import_item <new TImportItem> :
 "uses" "namespace"
  name:identifier <execute on_import>
  ( "in" path:string_literal )?
  ';'
  ;

import_sect <new TImportSect> :
  ( <add> import_item )* ;

/* unit */

unit_decl <new TUnitModule:TModule> :
   "namespace" name:identifier <execute on_module> ';'
   <new_line}
   '{'
   <new_line>
   intf_imports:import_sect
   impl_imports:import_sect
   <empty_line>

   intf_decl:decl_part
   <empty_line>

   impl_decl:decl_part
   ;

program_decl <new TProgramModule:TModule> :
   impl_imports:import_sect
   impl_decl:decl_part
   <new_line>
   "int" "main" '(' ')'  <new_line>
   '{'
   init:stat_list
   '}'
   ;
