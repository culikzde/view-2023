# oberon_comp.py

from __future__ import print_function

from pas_parser import *
from output import Output

# --------------------------------------------------------------------------

class Compiler (Parser) :
   def __init__ (self) :
       super (Compiler, self).__init__ ()

       self.current_type = None

       self.global_scope = TModule () # !?
       self.global_scope.item_name = ""
       self.global_scope.item_qual = ""
       self.global_scope.item_dict = { }
       self.global_scope.item_list = [ ]
       self.display = [ self.global_scope ]

   # scope

   def setupName (self, item) :
       # if len (self.display) == 0 :
       #    item.item_qual = item.item_name
       # else :
          top = self.display [-1]
          if top.item_qual == "" :
             item.item_qual = item.item_name
          else :
             item.item_qual = top.item_qual + "." + item.item_name

   def enter (self, item) :
       # if len (self.display) == 0 :
       #    self.display.append (item)
       # else :
          top = self.display [-1]
          top.item_dict [item.item_name] = item
          top.item_list.append (item)
          item.item_context = top

   def openScope (self, item) :
       if not hasattr (item, "item_dict") :
          item.item_dict = { }
          item.item_list = [ ]
       if not hasattr (item, "item_qual") :
          item.item_qual = ""
       self.display.append (item)

   def closeScope (self) :
       self.display.pop ()

   def lookup (self, name) :
       result = None
       inx = len (self.display) - 1
       while result == None and inx >= 0 :
          result = self.display [inx].item_dict.get (name)
          inx = inx - 1
       return result

   # module

   def open_module (self, module) :
       self.module = module

   def on_module (self, module) :
       module.item_name = module.name
       module.item_expand = True
       module.item_icon = "module"
       self.setupName (module)
       self.enter (module)
       self.markDefn (module)
       self.markOutline (module)
       self.openScope (module)
       self.openCompletion (module, outside = True)

   def on_import (self, param) :
       # param.item_name = param.name
       # self.markUsage (param)
       pass

   def close_module (self, module) :
       self.closeCompletion (module, outside = True)
       self.closeScope ()

   # class

   def open_class (self, cls) :
       self.named_type (cls)
       cls.item_expand = True
       cls.item_icon = "class"
       self.setupName (cls)
       self.markOutline (cls) # next/prev function/class
       self.openRegion (cls) # region with background color
       self.openScope (cls) # scope for identifiers
       self.openCompletion (cls, outside = True) # mark for text completion

   def close_class (self, cls) :
       self.closeCompletion (cls, outside = True)
       self.closeScope ()
       self.closeRegion ()

   def on_field (self, field) :
       field.item_name = field.name
       self.setupName (field)
       self.markDefn (field)

   def add_field (self, fields) :
       for field in fields.items :
          self.enter (field)

   # procedure

   def open_proc (self, proc) :
       pass

   def on_proc (self, proc) :
       if len (proc.proc_name.items) == 0 :
          proc.item_name = proc.proc_name.id
       else :
          proc.item_name = proc.proc_name.items[-1].id
       proc.item_icon = "function"
       self.setupName (proc)
       self.enter (proc)
       self.markDefn (proc)
       self.markOutline (proc)
       self.openRegion (proc)
       self.openScope (proc)
       self.openCompletion (proc, outside = True)

   def on_param (self, param) :
       param.item_name = param.name
       self.setupName (param)
       self.markDefn (param)

   def add_param (self, params) :
       for param in params.items :
           self.enter (param)

   def on_begin_proc (self, proc) :
       self.setInk ("lime")
       self.addToolTip ("begin of procedure " + proc.item_name)

   def on_end_proc (self, proc) :
       self.setInk ("red")
       self.addToolTip ("end of procedure " + proc.item_name)

   def close_proc (self, proc) :
       self.closeCompletion (proc, outside = True)
       self.closeScope ()
       self.closeRegion ()

   # constant declarations

   def on_const (self, const) :
       const.item_name = const.name
       self.setupName (const)
       self.markDefn (const)

   def add_const (self, const) :
       self.enter (const)

   # type declarations

   def on_type (self, type) :
       type.item_name = type.name
       self.current_type = type
       self.setupName (type)
       self.markDefn (type)

   def add_type (self, type) :
       self.enter (type)

   def simple_type (self, type) :
       self.current_type = None

   def named_type (self, type) :
       if self.current_type == None :
          self.error ("Class or interface without identifier")
       else :
          self.current_type.item_obj = type
          type.item_name = self.current_type.name
          self.current_type = None # value for nested types

   # variable declarations

   def on_var (self, var) :
       var.item_name = var.name
       var.item_icon = "variable"
       self.setupName (var)
       self.markDefn (var)

   def add_var (self, variables) :
       for var in variables.items :
           self.enter (var)

   # expressions

   def on_ident_expr (self, expr) :
       expr.item_decl = self.lookup (expr.name)
       if expr.item_decl != None :
          expr.item_qual = expr.item_decl.item_qual
       self.markUsage (expr)

   def on_field_expr (self, expr) :
       # expr.item_qual = self.lookup (expr.name) # !?
       # self.markUsage (expr)
       pass

# --------------------------------------------------------------------------

if __name__ == "__main__" :

   # PYTHONPATH=directory_with_generated_parser python pas_comp.py

   parser = Compiler ()
   parser.pascal = True
   parser.openFile ("../examples/example.pas")

   result = parser.parse_module_decl ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
