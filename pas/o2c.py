
# o2c.py

from oberon_parser import *
from output import Output

class O2C (Output) :

   def send_defined_name (self, param) :
      self.send (param.id)

   def send_qualified_name (self, param) :
      inx = 0
      cnt = len (param.items)
      self.send (param.id)
      while inx < cnt :
         self.send_cont_name (param.items [inx])
         inx = inx + 1

   def send_cont_name (self, param) :
      self.style_no_space ()
      self.send (".")
      self.style_no_space ()
      self.send (param.id)

   def send_expr_item (self, param) :
      self.send_expr (param.value)

   def send_expr_list (self, param) :
      inx = 0
      cnt = len (param.items)
      self.send_expr_item (param.items [inx])
      inx = inx + 1
      while inx < cnt :
         self.send (",")
         self.send_expr_item (param.items [inx])
         inx = inx + 1

   def send_elem_sect (self, param) :
      inx = 0
      cnt = len (param.items)
      self.send_elem_item (param.items [inx])
      inx = inx + 1
      while inx < cnt :
         self.send (",")
         self.send_elem_item (param.items [inx])
         inx = inx + 1

   def send_elem_item (self, param) :
      self.send_expr (param.low)
      if param.high != None :
         self.send ("..")
         self.send_expr (param.high)

   def send_ident_expr (self, param) :
      self.send (param.name)

   def send_int_value_expr (self, param) :
      self.send (param.value)

   def send_float_value_expr (self, param) :
      self.send (param.value)

   def send_string_value_expr (self, param) :
      self.sendStr (param.value)

   def send_char_value_expr (self, param) :
      self.sendChr (param.value)

   def send_nil_expr (self, param) :
      self.send ("NULL")

   def send_true_expr (self, param) :
      self.send ("true")

   def send_false_expr (self, param) :
      self.send ("false")

   def send_sub_expr (self, param) :
      self.send ("(")
      self.send_expr (param.value)
      self.send (")")

   def send_not_expr (self, param) :
      self.send ("!")
      self.send_factor (param.param)

   def send_plus_expr (self, param) :
      self.send ("+")
      self.send_factor (param.param)

   def send_minus_expr (self, param) :
      self.send ("-")
      self.send_factor (param.param)

   def send_not_expr (self, param) :
      self.send ("~")
      self.send_factor (param.param)

   def send_set_expr (self, param) :
      self.send ("[")
      self.send_elem_sect (param.list)
      self.send ("]")

   def send_variable (self, param) :
      if isinstance (param, TIdentExpr) :
         self.send_ident_expr (param)
      elif isinstance (param, TStructExpr) :
         self.send_sub_expr (param)

   def send_reference (self, param) :
      if isinstance (param, TIndexExpr) :
         self.send_reference (param.left)
         self.send ("[")
         self.send_expr_list (param.value_list)
         self.send ("]")
      elif isinstance (param, TCallExpr) :
         self.send_reference (param.func)
         self.send ("(")
         self.send_expr_list (param.value_list)
         self.send (")")
      elif isinstance (param, TDerefExpr) :
         left = param.param
         self.send ("*")
         self.style_no_space ()
         self.send ("(")
         self.send_reference (left)
         self.send (")")
      elif isinstance (param, TFieldExpr) :
         left = param.param
         if isinstance (left, TDerefExpr) :
            self.send_reference (left.param)
            self.style_no_space ()
            self.send ("->")
            self.style_no_space ()
            self.send (param.name)
         else :
            self.send_reference (left)
            self.style_no_space ()
            self.send (".")
            self.style_no_space ()
            self.send (param.name)
      else :
         self.send_variable (param)

   def send_factor (self, param) :
      if isinstance (param, TIntValueExpr) :
         self.send_int_value_expr (param)
      elif isinstance (param, TFltValueExpr) :
         self.send_float_value_expr (param)
      elif isinstance (param, TStrValueExpr) :
         self.send_string_value_expr (param)
      elif isinstance (param, TPlusExpr) :
         self.send_plus_expr (param)
      elif isinstance (param, TMinusExpr) :
         self.send_minus_expr (param)
      elif isinstance (param, TNotExpr) :
         self.send_not_expr (param)
      elif isinstance (param, TNilExpr) :
         self.send_nil_expr (param)
      elif isinstance (param, TTrueExpr) :
         self.send_true_expr (param)
      elif isinstance (param, TFalseExpr) :
         self.send_false_expr (param)
      elif isinstance (param, TSetExpr) :
         self.send_set_expr (param)
      elif isinstance (param, TExpr) :
         self.send_reference (param)

   def send_term (self, param) :
      if isinstance (param, TTermExpr) :
         # if param.fce == param.AsExp :
         #    self.send ("dynamic_cast")
         #    self.send ("<")
         #    self.style_no_space ()
         #    self.send_factor (param.right)
         #    self.style_no_space ()
         #    self.send ("*")
         #    self.style_no_space ()
         #    self.send (">")
         #    self.send ("(")
         #    self.send_term (param.left)
         #    self.send (")")

         self.send_term (param.left)
         if param.fce == param.MulExp :
            self.send ("*")
         elif param.fce == param.RDivExp :
            self.send ("/")
         elif param.fce == param.DivExp :
            self.send ("/")
         elif param.fce == param.ModExp :
            self.send ("%")
         # elif param.fce == param.ShlExp :
         #    self.send ("<<")
         # elif param.fce == param.ShrExp :
         #    self.send (">>")
         elif param.fce == param.AndExp :
            self.send ("&&")
         self.send_factor (param.right)
      else :
         self.send_factor (param)

   def send_simple_expr (self, param) :
      if isinstance (param, TSimpleExpr) :
         self.send_simple_expr (param.left)
         if param.fce == param.AddExp :
            self.send ("+")
         elif param.fce == param.SubExp :
            self.send ("-")
         elif param.fce == param.OrExp :
            self.send ("||")
         # elif param.fce == param.XorExp :
         #    self.send ("^")
         self.send_term (param.right)
      else :
         self.send_term (param)

   def send_expr (self, param) :
      if isinstance (param, TCompleteExpr) :
         if param.fce == param.IsExp :
            self.send ("dynamic_cast")
            self.send ("<")
            self.style_no_space ()
            self.send_factor (param.right)
            self.style_no_space ()
            self.send ("*")
            self.style_no_space ()
            self.send (">")
            self.send ("(")
            self.send_term (param.left)
            self.send (")")
            self.send ("!=")
            self.send ("NULL")
         else :
            self.send_simple_expr (param.left)
            if param.fce == param.EqExp :
               self.send ("=")
            elif param.fce == param.NeExp :
               self.send ("!=")
            elif param.fce == param.LtExp :
               self.send ("<")
            elif param.fce == param.GtExp :
               self.send (">")
            elif param.fce == param.LeExp :
               self.send ("<=")
            elif param.fce == param.GeExp :
               self.send (">=")
            elif param.fce == param.InExp :
               self.send ("in")
            self.send_simple_expr (param.right)
      else :
         self.send_simple_expr (param)

   def send_stat_list (self, param) :
      self.send ("{")
      self.style_new_line ()
      self.style_indent ()
      for item in param.items :
         self.style_new_line ()
         self.send_stat (item)
      self.style_unindent ()
      self.style_new_line ()
      self.send ("}")
      self.style_new_line ()

   def send_if_stat (self, param) :
      self.send ("if")
      self.send ("(")
      self.send_expr (param.cond_expr)
      self.send (")")
      self.send_stat_list (param.then_stat)
      for item in param.elseif_stat :
         self.send ("else if")
         self.send ("(")
         self.send_expr (item.cond_expr)
         self.send (")")
         self.send_stat_list (item.cond_stat)
      if param.else_stat != None :
         self.send ("else")
         self.send_stat_list (param.else_stat)

   def send_case_stat (self, param) :
      self.send ("switch")
      self.send ("(")
      self.send_expr (param.case_expr)
      self.send (")")
      self.style_new_line ()
      self.send ("{")
      self.send_case_sect (param.case_list)
      self.send ("}")

   def send_case_sect (self, param) :
      inx = 0
      cnt = len (param.items)
      self.send_case_item (param.items [inx])
      inx = inx + 1
      while inx < cnt :
         self.send (";")
         self.style_new_line ()
         self.send_case_item (param.items [inx])
         inx = inx + 1

   def send_case_item (self, param) :
      self.send ("case")
      self.send_elem_sect (param.sel_list)
      self.send (":")
      self.send_stat_list (param.sel_stat)

   def send_while_stat (self, param) :
      self.send ("while")
      self.send ("(")
      self.send_expr (param.cond_expr)
      self.send (")")
      self.send_stat_list (param.body_stat)

   def send_repeat_stat (self, param) :
      self.send ("do")
      self.send ("{")
      self.send_stat_list (param.body_seq)
      self.send ("}")
      self.style_new_line ()
      self.send ("while")
      self.send ("(")
      self.send ("!")
      self.send ("(")
      self.send_expr (param.until_expr)
      self.send (")")
      self.send (")")

   def send_for_stat (self, param) :
      self.send ("for")
      self.send ("(")

      self.send_variable (param.var_expr)
      self.send ("=")
      self.send_expr (param.from_expr)
      self.send (";")

      self.send_variable (param.var_expr)
      if param.incr :
         self.send ("<=")
      else  :
         self.send (">=")
      self.send_expr (param.to_expr)
      self.send (";")

      self.send_variable (param.var_expr)
      if param.incr :
         self.send ("++")
      else  :
         self.send ("--")

      self.send (")")
      self.send_inner_stat (param.body_stat)

   def send_simple_stat (self, param) :
      if isinstance (param, TAssignStat) :
         self.send_expr (param.left_expr)
         self.send_assign_stat (param)
      elif isinstance (param, TCallStat) :
         self.send_expr (param.call_expr)
         self.send_call_stat (param)
      else :
         self.send_expr (param)

   def send_assign_stat (self, param) :
      self.send ("=")
      self.send_expr (param.right_expr)
      self.send (";")

   def send_call_stat (self, param) :
      self.send (";")

   def send_labeled_stat (self, param) :
      self.send_label_ident (param.lab)
      self.send (":")
      self.send_stat (param.body)

   def send_stat (self, param) :
      self.openSection (param)
      if isinstance (param, TIfStat) :
         self.send_if_stat (param)
      elif isinstance (param, TCaseStat) :
         self.send_case_stat (param)
      elif isinstance (param, TWhileStat) :
         self.send_while_stat (param)
      elif isinstance (param, TRepeatStat) :
         self.send_repeat_stat (param)
      elif isinstance (param, TForStat) :
         self.send_for_stat (param)
      elif isinstance (param, TStat) :
         self.send_simple_stat (param)
      self.closeSection ()

   def send_param_ident (self, param) :
      self.send (param.name)

   def send_param_item (self, param) :
      inx = 0
      cnt = len (param.items)
      while inx < cnt :
         if param.typ != None :
            self.send_param_type (param.typ)
         if param.var_param :
            self.send ("&")
         self.send_param_ident (param.items [inx])
         inx = inx + 1
         if inx < cnt :
            self.send (",")

   def send_formal_param_list (self, param) :
      inx = 0
      cnt = len (param.items)
      self.send ("(")
      if inx < cnt :
         self.send_param_item (param.items [inx])
         inx = inx + 1
         if inx < cnt :
            self.send (",")
      self.send (")")

   def send_class_type (self, param) :
      self.send ("class")
      if param.parent != None :
         self.send (":")
         self.send ("public")
         self.send_ifc_type (param.parent)
         if param.ifc_list != None :
            self.send (",")
            self.send_interface_sect (param.ifc_list)
      self.style_new_line ()
      self.send ("{")
      self.send_components_sect (param.components)
      self.send ("}")

   def send_field_decl (self, param) :
      for item in param.items :
         self.style_new_line ()
         self.send_type (param.typ)
         self.send (item.name)
         self.send (";")

   def send_array_type (self, param) :
      self.send ("array")
      if param.index_list != None :
         self.send ("[")
         self.send_index_type_sect (param.index_list)
         self.send ("]")
      self.send ("of")
      self.send_type (param.elem)

   def send_index_type_sect (self, param) :
      inx = 0
      cnt = len (param.items)
      self.send_index_type_item (param.items [inx])
      inx = inx + 1
      while inx < cnt :
         self.send (",")
         self.send_index_type_item (param.items [inx])
         inx = inx + 1

   def send_index_type_item (self, param) :
      self.send_type (param.index)

   def send_record_type (self, param) :
      self.send ("struct")
      self.send ("{")
      for item in param.fields.items :
         self.send_field_decl (item)
      self.send ("}")

   def send_pointer_type (self, param) :
      self.send ("*")
      self.send_type (param.elem)

   def send_proc_type (self, param) :
       self.send ("procedure")
       self.send_formal_param_list (param.param_list)
       if param.answer != None :
          self.send (":")
          self.send_result_type (param.answer)

   def send_alias_type (self, param) :
      self.send_qualified_name (param.alias_name)

   def send_type (self, param) :
      if isinstance (param, TArrayType) :
         self.send_array_type (param)
      elif isinstance (param, TRecordType) :
         self.send_record_type (param)
      elif isinstance (param, TPointerType) :
         self.send_pointer_type (param)
      elif isinstance (param, TProcType) :
         self.send_proc_type (param)
      elif isinstance (param, TAliasType) :
         self.send_alias_type (param)

   def send_const_decl (self, param) :      
      self.send ("const")
      if param.typ != None :
         self.send_type (param.typ)
      self.send (param.name)
      self.send ("=")
      self.send_expr (param.val)
      self.send (";")

   def send_const_sect (self, param) :
      for item in param.items :
         self.send_const_decl (item)

   def send_type_decl (self, param) :
      self.send ("typedef")
      self.send_type (param.typ)
      self.send (param.name)
      self.send (";")

   def send_type_sect (self, param) :
      for item in param.items :
         self.send_type_decl (item)

   def send_var_decl (self, param) :
      for item in param.items :
         self.style_new_line ()
         self.openSection (item)
         self.send_type (param.typ)
         self.send (item.name)
         self.send (";")
         self.closeSection ()

   def send_var_sect (self, param) :
      for item in param.items :
         self.send_var_decl (item)

   def send_decl_head (self, param) :
      if param.answer == None :
         self.send ("void")
      else :
         self.send_result_type (param.answer)
      self.send_qualified_name (param.proc_name)
      self.send_formal_param_list (param.param_list)
      self.style_new_line ()
      self.send ("{")
      self.style_indent ()
      self.send_decl_part (param.decl)
      self.style_unindent ()
      self.send_stat_list (param.body)
      self.send ("}")
      self.style_empty_line ()

   def send_decl (self, param) :
      self.openSection (param)
      if isinstance (param, TConstSect) :
         self.send_const_sect (param)
      elif isinstance (param, TTypeSect) :
         self.send_type_sect (param)
      elif isinstance (param, TVarSect) :
         self.send_var_sect (param)
      elif isinstance (param, TProcDecl) :
         self.send_proc_decl (param)
      self.closeSection ()

   def send_decl_part (self, param) :
      inx = 0
      cnt = len (param.items)
      while inx < cnt :
         self.style_new_line ()
         self.send_decl (param.items [inx])
         inx = inx + 1

   def send_import_item (self, param) :
      self.send (param.name)
      if param.from_name != "" :
         self.send ("in")
         self.send (param.from_name)

   def send_import_sect (self, param) :
      inx = 0
      cnt = len (param.items)
      if inx < cnt :
         self.style_empty_line ()
         self.send ("uses")
         self.send_import_item (param.items [inx])
         inx = inx + 1
         while inx < cnt :
            self.send (",")
            self.send_import_item (param.items [inx])
            inx = inx + 1
         self.send (";")
         self.style_empty_line ()

   def send_module_decl (self, param) :
      self.openSection (param)
      self.send_import_sect (param.imports)
      self.style_empty_line ()
      self.send_import_sect (param.impl_imports)
      self.send_decl_part (param.impl_decl)
      if param.init != None :
         self.send_stat_list (param.init)
      self.closeSection ()

# --------------------------------------------------------------------------

if __name__ == "__main__" :

   # PYTHONPATH=directory_with_generated_parser python p2c.py

   parser = Parser ()
   parser.pascal = True
   parser.openFile ("pas/examples/idedemo.pas")

   result = parser.parse_module_decl ()

   product = P2C ()
   product.send_module_decl (result)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
